//
//  CLYAppDelegate.h
//  Communly
//
//  Created by Chris Allwein on 3/10/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void) resetLogin;


@end
