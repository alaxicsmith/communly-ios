//
//  CLYExploreCell.h
//  Communly
//
//  Created by Chris Allwein on 7/10/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYExploreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *communityTagLabel;
@property (weak, nonatomic) IBOutlet UIImageView *communityAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *communityBackgroundImageView;

@property (strong, nonatomic) NSString *communityId;

@end
