//
//  CLYExploreCommunitiesViewController.m
//  Communly
//
//  Created by Chris Allwein on 4/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYExploreCommunitiesViewController.h"
#import "CLYCommunityViewController.h"
#import "CLYCommunityWebService.h"
#import "CLYExploreCell.h"
#import "CLYCommunity.h"
#import "CLYApplicationConstants.h"
#import "UIImageView+AFNetworking.h"

@interface CLYExploreCommunitiesViewController ()

@property(nonatomic,strong)NSArray *popular;
@property(nonatomic,strong)NSArray *verified;
@property(nonatomic,strong)NSArray *recent;

@end

@implementation CLYExploreCommunitiesViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.translucent = NO;
    self.title = @"Explore";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    self.popular = [NSArray array];
    self.verified = [NSArray array];
    self.recent = [NSArray array];
    
    self.tableView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    [[CLYCommunityWebService sharedService] getPopularCommunitiesWithCompletionHandler:^(NSArray *communities) {
        self.popular = communities;
        [self.tableView reloadData];
    }];
    
    [[CLYCommunityWebService sharedService] getVerifiedCommunitiesWithCompletionHandler:^(NSArray *communities) {
        self.verified = communities;
        [self.tableView reloadData];
    }];
    
    [[CLYCommunityWebService sharedService] getRecentCommunitiesWithCompletionHandler:^(NSArray *communities) {
        self.recent = communities;
        [self.tableView reloadData];
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

#pragma mark TableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [self.popular count];
            break;
            
        case 1:
            return [self.verified count];
            break;
            
        case 2:
            return [self.recent count];
            break;
            
        default:
            break;
    }

    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYExploreCell *cell = (CLYExploreCell*)[tableView dequeueReusableCellWithIdentifier:@"CLYExploreCell"];
    
    CLYCommunity *community;
    
    switch (indexPath.section) {
        case 0:
            community = [self.popular objectAtIndex:indexPath.row];
            break;
            
        case 1:
            community = [self.verified objectAtIndex:indexPath.row];
            break;
            
        case 2:
            community = [self.recent objectAtIndex:indexPath.row];
            break;
            
        default:
            break;
    }
    
    cell.communityNameLabel.text = community.name;
//    cell.memberCountLabel.text = [NSString stringWithFormat:@"%@ Members", @"2"];
    cell.memberCountLabel.text = [NSString stringWithFormat:@"%@ Members", community.communityMemberCount];
    cell.communityTagLabel.text = community.category;
    
    cell.communityNameLabel.textColor = [UIColor whiteColor];
    cell.memberCountLabel.textColor = [UIColor whiteColor];
    
    cell.communityId = community.id;
    
    [cell.communityAvatarImageView setImageWithURL:[NSURL URLWithString:community.avatar]];
    
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:community.background]];
    [cell.communityBackgroundImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        cell.communityBackgroundImageView.image = image;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    
    
    
    return cell;
 
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYCommunity *community;
    
    switch (indexPath.section) {
        case 0:
            community = [self.popular objectAtIndex:indexPath.row];
            break;
            
        case 1:
            community = [self.verified objectAtIndex:indexPath.row];
            break;
            
        case 2:
            community = [self.recent objectAtIndex:indexPath.row];
            break;
            
        default:
            break;
    }
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
    CLYCommunityViewController *vc = [story instantiateViewControllerWithIdentifier:@"CLYCommunityViewController"];
    vc.communityId = community.id;
    [self.navigationController pushViewController:vc animated:YES];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"footer"];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 47.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 47)];
    header.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 305, 43)];
    label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:15.0];
    label.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0];
    
    
    switch (section) {
        case 0:
            label.text = @"Popular Communities";
            break;
            
        case 1:
            label.text = @"Verified Communities";
            break;
            
        case 2:
            label.text = @"Recent Communities";
            break;
            
        default:
            break;
    }
    
    [header addSubview:label];
    return header;
}




@end
