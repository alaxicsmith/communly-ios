//
//  CLYExploreCell.m
//  Communly
//
//  Created by Chris Allwein on 7/10/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYExploreCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation CLYExploreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.memberCountLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    self.communityTagLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    
    self.communityNameLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:16.0];
    self.communityTagLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
//    self.bannerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    
    self.communityAvatarImageView.layer.cornerRadius = 3.0;
    self.communityAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.communityAvatarImageView.layer.borderWidth = 1.0;
    self.communityAvatarImageView.clipsToBounds = YES;

    
    self.communityBackgroundImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
