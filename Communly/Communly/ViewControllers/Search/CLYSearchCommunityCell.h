//
//  CLYSearchCommunityCell.h
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYSearchCell.h"

@interface CLYSearchCommunityCell : CLYSearchCell
@property (nonatomic,weak) IBOutlet UIImageView *headerImageView;
@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic,weak) IBOutlet UIButton *actionButton;

- (IBAction)actionButtonTapped:(id)sender;

@property (nonatomic,strong) NSString *communityId;
@end
