//
//  CLYSearchViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSearchViewController.h"
#import "MMDrawerBarButtonItem.h"
#import "CLYUser.h"
#import "CLYCommunity.h"
#import "CLYPost.h"
#import "CLYSearchWebService.h"
#import "CLYSearchCommunityCell.h"
#import "CLYSearchPeopleCell.h"
#import "CLYSearchPostCell.h"
#import "CLYSearchNoResultsCell.h"
#import "UIImageView+AFNetworking.h"
#import "CLYCommunityViewController.h"
#import "CLYPostViewController.h"
#import "CLYUserViewController.h"
#import "AFOAuth2Client.h"
#import "CLYAppDelegate.h"

@interface CLYSearchViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSDictionary *searchResults;
@property (strong, nonatomic) NSMutableArray *communities;
@property (strong, nonatomic) NSMutableArray *posts;
@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *searchFilterControl;
- (IBAction)searchFilterChanged:(id)sender;
@end

@implementation CLYSearchViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.communities = [NSMutableArray array];
    self.posts = [NSMutableArray array];
    self.users = [NSMutableArray array];
    
    self.searchResults = @{@"communities":self.communities,@"users":self.users,@"posts":self.posts};
    
    self.searchDisplayController.displaysSearchBarInNavigationBar=YES;
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.image = [UIImage imageNamed:@"hamburger"];
    leftDrawerButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIdentifier"];
    self.searchBar.text = self.searchString;
    //TODO: Perform search on load.
    
    
    [self.searchFilterControl setBackgroundImage:[self imageWithColor] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.searchFilterControl setBackgroundImage:[self imageWithColor] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.searchFilterControl setBackgroundImage:[self imageWithColor] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    self.searchFilterControl.backgroundColor =[UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    
    [self.searchFilterControl setDividerImage:[self imageWithColor] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.searchFilterControl setDividerImage:[self imageWithColor] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.searchFilterControl setDividerImage:[self imageWithColor] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

    self.searchFilterControl.tintColor = [UIColor blackColor];
    [self.searchFilterControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:123.0/255.0 green:123.0/255.0 blue:123.0/255.0 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:13.0]} forState:UIControlStateNormal];
    [self.searchFilterControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:77.0/255.0 green:77.0/255.0 blue:77.0/255.0 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:13.0]} forState:UIControlStateSelected];
    [self.searchFilterControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:77.0/255.0 green:77.0/255.0 blue:77.0/255.0 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:13.0]} forState:UIControlStateHighlighted];
    

    
    self.searchBar.frame = CGRectMake(0, 0, 320, 40);
    self.navigationItem.titleView.tintColor = [UIColor blackColor];
    
    if (self.searchString && [self.searchString isEqualToString:@"logout"]) {
        CLYAppDelegate *ad = (CLYAppDelegate*)[UIApplication sharedApplication].delegate;
        [ad resetLogin];
        
    }

    [[CLYSearchWebService sharedService] getSearchResultsWithQuery:self.searchString completionHandler:^(NSDictionary *results) {
        self.communities = [results objectForKey:@"communities"];
        self.posts = [results objectForKey:@"posts"];
        self.users = [results objectForKey:@"users"];
        
        self.searchResults = @{@"communities":self.communities,@"users":self.users,@"posts":self.posts};
        
        [self.tableView reloadData];
    }];
}

-(void)openCommunity:(NSString*)communityId
{
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
    CLYCommunityViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYCommunityViewController"];
    vc.communityId = communityId;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)openUser:(NSString*)userId
{
    UIStoryboard *user = [UIStoryboard storyboardWithName:@"User" bundle:nil];
    CLYUserViewController *vc = [user instantiateViewControllerWithIdentifier:@"CLYUserViewController"];
    vc.userId = userId;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)openPost:(NSString*)postId
{
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"Post" bundle:nil];
    CLYPostViewController *vc = [post instantiateViewControllerWithIdentifier:@"CLYPostViewController"];
    vc.postId = postId;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UIImage *)imageWithColor{
    UIColor *color = [UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 40.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==1) {
                if ([self.communities count]> 0) {
                    return [self.communities count];
                }else{
                    return 1;
                }
            }
            break;
        case 1:
            if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==2) {
                if ([self.users count]> 0) {
                    return [self.users count];
                }else{
                    return 1;
                }
            }
            break;
        case 2:
            if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==3) {
                if ([self.posts count]> 0) {
                    return [self.posts count];
                }else{
                    return 1;
                }
            }
            break;
            
        default:
            break;
    }
    
    return 0;
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0) {
        if ([self.communities count]==0) {
            CLYSearchNoResultsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchNoResultsCell"];
            cell.titleLabel.text = @"No Results";
            return cell;
        }
        
        CLYSearchCommunityCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchCommunityCell"];
        CLYCommunity *community = [self.communities objectAtIndex:indexPath.row];
        
        [cell.headerImageView setImageWithURL:[NSURL URLWithString:community.avatar]];
        cell.titleLabel.text = community.name;
        cell.subtitleLabel.text = [NSString stringWithFormat:@"%@ members",community.communityMemberCount];
        [cell.actionButton setTitle:@"Join" forState:UIControlStateNormal];
        cell.communityId = community.id;
        return cell;
        
    } else if (indexPath.section == 1) {
        if ([self.users count]==0) {
            CLYSearchNoResultsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchNoResultsCell"];
            cell.titleLabel.text = @"No Results";
            return cell;
        }

        CLYSearchPeopleCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchPeopleCell"];
        CLYUser *user = [self.users objectAtIndex:indexPath.row];
        
        [cell.headerImageView setImageWithURL:[NSURL URLWithString:user.avatar]];
        cell.titleLabel.text = user.name;
        cell.subtitleLabel.text = user.location;
        [cell.actionButton setTitle:@"Follow" forState:UIControlStateNormal];
        cell.userId = user.id;
        return cell;
        
    }else if (indexPath.section == 2) {
        if ([self.posts count]==0) {
            CLYSearchNoResultsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchNoResultsCell"];
            cell.titleLabel.text = @"No Results";
            return cell;
        }

        CLYSearchPostCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CLYSearchPostCell"];
        CLYPost *post = [self.posts objectAtIndex:indexPath.row];
        
        [cell.headerImageView setImageWithURL:[NSURL URLWithString:post.photo]];
        cell.titleLabel.text = post.content;
        cell.subtitleLabel.text = [NSString stringWithFormat:@"%i Faves", [post.favesCount intValue] ];
        [cell.actionButton setTitle:@"Comment" forState:UIControlStateNormal];
        return cell;
        
    }

	return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CLYCommunity *community = [self.communities objectAtIndex:indexPath.row];
        [self openCommunity:community.id];
    }

    if (indexPath.section == 1) {
        CLYUser *user = [self.users objectAtIndex:indexPath.row];
        [self openUser:user.id];
    }

    if (indexPath.section == 2) {
        CLYPost *post = [self.posts objectAtIndex:indexPath.row];
        [self openPost:post.id];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    header.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    UIView *top = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    top.backgroundColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0];
    [header addSubview:top];

    UIView *bottom = [[UIView alloc] initWithFrame:CGRectMake(0, 29, 320, 1)];
    bottom.backgroundColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0];
    [header addSubview:bottom];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 20)];
    label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11];
    label.textColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    if (section == 0) {
        label.text =  @"Communities";
    } else if (section == 1) {
                label.text =  @"Users";
    }else if (section == 2) {
                label.text =  @"Posts";
    }
    
    [header addSubview:label];
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
  if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==1) {
        return 30;
  }
    } else if (section == 1) {
        if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==2) {
            return 30;
        }
    }else if (section == 2) {
        if (self.searchFilterControl.selectedSegmentIndex == 0 || self.searchFilterControl.selectedSegmentIndex==3) {
            return 30;
        }
    }
    
    return 0;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
//    NSLog(@"Search button clicked");
    self.searchString = self.searchBar.text;
    [[CLYSearchWebService sharedService] getSearchResultsWithQuery:self.searchString completionHandler:^(NSDictionary *results) {
        self.communities = [results objectForKey:@"communities"];
        self.posts = [results objectForKey:@"posts"];
        self.users = [results objectForKey:@"users"];
        
        self.searchResults = @{@"communities":self.communities,@"users":self.users,@"posts":self.posts};
        
        [self.tableView reloadData];

    }];

}



- (IBAction)searchFilterChanged:(id)sender {
    [self.tableView reloadData];
    
    
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
//    NSLog(@"didHideSearchResultsTableView");

    // undo the changes above to prevent artefacts reported below by mclin
}

-(void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
//    NSLog(@"didShowSearchResultsTableView");
    tableView.hidden = YES;
    [self.view bringSubviewToFront:self.tableView];
    [self.view bringSubviewToFront:self.searchFilterControl];
}


@end
