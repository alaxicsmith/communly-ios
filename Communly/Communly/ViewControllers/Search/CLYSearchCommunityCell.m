//
//  CLYSearchCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSearchCommunityCell.h"
#import "CLYCommunityWebService.h"

@implementation CLYSearchCommunityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)actionButtonTapped:(id)sender {
//    NSLog(@"Search Community Action Button Tapped");
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"Join"]) {
        [[CLYCommunityWebService sharedService] joinCommunityWithId:self.communityId];
        [button setTitle:@"Unjoin" forState:UIControlStateNormal];
    }else{
        [[CLYCommunityWebService sharedService] unjoinCommunityWithId:self.communityId];
        [button setTitle:@"Join" forState:UIControlStateNormal];
    }
    
}
@end
