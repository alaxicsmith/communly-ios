//
//  CLYSearchCell.h
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYSearchCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *headerImageView;
@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic,weak) IBOutlet UIButton *actionButton;

@end
