//
//  CLYSearchPeopleCell.m
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSearchPeopleCell.h"
#import "CLYUserWebService.h"
#import "CLYApplicationConstants.h"

@implementation CLYSearchPeopleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)actionButtonTapped:(id)sender {
    UIButton *button = (UIButton*)sender;
//    NSLog(@"Search People Action Button Tapped");
    if ([button.titleLabel.text isEqualToString:@"Follow"]) {
        [[CLYUserWebService sharedService] followUserWithId:self.userId];
        [button setTitle:@"Following" forState:UIControlStateNormal];
        button.backgroundColor = [UIColor lightGrayColor];
    }else{
        [[CLYUserWebService sharedService] unfollowUserWithId:self.userId];
        [button setTitle:@"Follow" forState:UIControlStateNormal];
        button.backgroundColor = [CLYApplicationConstants redButtonColor];
    }

}
@end
