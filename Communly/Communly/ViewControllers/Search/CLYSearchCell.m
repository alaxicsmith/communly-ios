//
//  CLYSearchCell.m
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSearchCell.h"
#import "CLYApplicationConstants.h"
#import <QuartzCore/QuartzCore.h>

@implementation CLYSearchCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.headerImageView.layer.cornerRadius = 3.0;
    self.headerImageView.layer.masksToBounds = YES;
    
    self.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13];

    self.subtitleLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:10];

    
    self.actionButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:10];
    self.actionButton.layer.cornerRadius = 3.0;
    self.actionButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    self.actionButton.tintColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
