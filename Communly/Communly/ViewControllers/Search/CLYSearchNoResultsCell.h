//
//  CLYSearchNoResultsCell.h
//  Communly
//
//  Created by Chris Allwein on 6/24/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYSearchNoResultsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
