//
//  CLYCommunityViewController.m
//  Communly
//
//  Created by Chris Allwein on 4/4/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCommunityViewController.h"
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"
#import "CLYCommunityHeaderCell.h"
#import "CLYCommunity.h"
#import "CLYCommunityWebService.h"
#import "CLYPostsWebService.h"
#import "UIImageView+AFNetworking.h"
#import "CLYStreamCell.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYPostViewController.h"
#import "CLYCommentTextField.h"
#import "CLYApplicationConstants.h"
#import "CLYHelperFunctions.h"
#import "CLYUserViewController.h"

@interface CLYCommunityViewController ()<UITextFieldDelegate>

@property (nonatomic,strong) CLYCommunity *community;
@property (nonatomic,strong) NSArray *posts;

@property (nonatomic,strong) UITextField *hiddenCommentTextField;
@property (nonatomic,strong) UIView *commentAccessoryView;
@property (nonatomic,strong) CLYCommentTextField *commentTextField;
@property (nonatomic,strong) UIButton *commentButton;
@property (nonatomic,strong) NSString *commentPost;


@end

@implementation CLYCommunityViewController

-(void)setCommunityId:(NSString *)communityId
{
        self.tableView.hidden = YES;
    _communityId = communityId;
    [[CLYCommunityWebService sharedService] getCommunityWithId:self.communityId completionHandler:^(CLYCommunity *community) {
        self.community = community;
        [self.tableView reloadData];
            self.tableView.hidden = NO;
    }];
    
    [[CLYPostsWebService sharedService] getPostsWithCommunityId:self.communityId  completionHandler:^(NSArray *posts) {
        self.posts = posts;
        [self.tableView reloadData];
    }];

}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.image = [UIImage imageNamed:@"hamburger"];
    leftDrawerButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    UIBarButtonItem *composeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"compose"] style:UIBarButtonItemStylePlain target:self action:@selector(compose)];
    composeButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 15);
    [self.navigationItem setRightBarButtonItem:composeButton animated:YES];
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createComment:) name:@"CREATE_COMMENT" object:nil];
    [self registerForKeyboardNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openUser:) name:@"OPEN_USER" object:nil];

}

-(void) createComment:(NSNotification*)sender
{
//    NSLog(@"Showing comment entry field");
    _commentAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _commentAccessoryView.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    _commentTextField = [[CLYCommentTextField alloc] initWithFrame:CGRectMake(8, 8, 223, 28)];
    _commentTextField.delegate = self;
    
    
    _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _commentButton.frame = CGRectMake(239, 8, 73, 28);
    NSAttributedString *attrButtonTitle = [[NSAttributedString alloc] initWithString:@"Comment" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-SemiBold" size:12.0], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [_commentButton setAttributedTitle:attrButtonTitle forState:UIControlStateNormal];
    _commentButton.layer.cornerRadius = 3.0;
    _commentButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    [_commentButton addTarget:self action:@selector(saveComment) forControlEvents:UIControlEventTouchUpInside];
    
    [_commentAccessoryView addSubview:_commentTextField];
    [_commentAccessoryView addSubview:_commentButton];
    
    _hiddenCommentTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _hiddenCommentTextField.delegate = self;
    _commentPost = sender.object;
    [self.view.window addSubview:_hiddenCommentTextField];
    [_hiddenCommentTextField becomeFirstResponder];
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self saveComment];
    return YES;
}

#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    [_commentTextField becomeFirstResponder];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    // Now add the view as an input accessory view to the selected textfield.
    if (textField != _commentTextField) {
        [textField setInputAccessoryView:_commentAccessoryView];
        
    }
    
}

-(void)saveComment
{
//    NSLog(@"Saving Comment: %@", _commentTextField.text);
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
    [[CLYPostsWebService sharedService] commentOnPost:_commentPost user:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] content:_commentTextField.text completionHandler:^{
        
    }];
    
}


-(void)compose
{
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"CreatePost" bundle:nil];
    UIViewController *vc = [post instantiateInitialViewController];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else
    {
        return [self.posts count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CLYCommunityHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLYCommunityHeaderCell" forIndexPath:indexPath];
        cell.communityId = self.community.id;
        cell.communityNameLabel.text = self.community.name;
        cell.communityTypeLabel.text = self.community.category;
        [cell.communityBackgroundImageView setImageWithURL:[NSURL URLWithString:self.community.background]];
        [cell.communityAvatarImageView setImageWithURL:[NSURL URLWithString:self.community.avatar]];
        
        cell.memberCountLabel.text = [NSString stringWithFormat:@"%@ members",self.community.communityMemberCount];
        return cell;
    }else
    {
        CLYPost *object = [self.posts objectAtIndex:indexPath.row];
        
        CLYStreamCell *streamCell = (CLYStreamCell*)[tableView dequeueReusableCellWithIdentifier:@"CLYStreamCell" forIndexPath:indexPath];
        streamCell.footerView.communityButton.hidden = YES;
        
        [streamCell.headerView.userImage setImageWithURL:[NSURL URLWithString:object.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
        streamCell.headerView.userNameLabel.text = object.user.name;
        streamCell.headerView.userId = object.user.id;
        streamCell.headerView.timeLabel.text = [CLYHelperFunctions timePostedFormatted:object.created_at];
        
        if (object.photo && object.photo.length > 0 && ![object.photo isEqualToString:@"/photos/original/missing.png"]) {
            [streamCell.postImageView setImageWithURL:[NSURL URLWithString:object.photo]];
            streamCell.postImageView.contentMode = UIViewContentModeScaleAspectFill;
            streamCell.heightConstraint.constant = 275.0;
            streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
            streamCell.headerView.userNameLabel.textColor = [UIColor whiteColor];
            streamCell.headerView.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0];
            streamCell.headerView.alpha = 0.7;
            streamCell.headerView.timeLabel.textColor = [UIColor whiteColor];
            [self.view layoutIfNeeded];
        }else{
            streamCell.postImageView.image = nil;
            streamCell.heightConstraint.constant = 62.0;
            streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:15.0];
            streamCell.headerView.userNameLabel.textColor = [UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0];
            
            streamCell.headerView.timeLabel.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0];
            streamCell.headerView.backgroundColor = [UIColor clearColor];
            streamCell.headerView.alpha = 1.0;
            [self.view layoutIfNeeded];
        }
        streamCell.footerView.postTextLabel.text = object.content;
        streamCell.footerView.communityId = object.community.id;
        streamCell.footerView.postId = object.id;
        streamCell.headerView.userId = object.user.id;

        streamCell.footerView.favesButton.favesCount = [object.favesCount intValue];
        
        return streamCell;
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 225;
    }
    
    CLYPost *post = [self.posts objectAtIndex:indexPath.row];
    
    CGFloat header = 62.0;
    CGFloat image;
    
    if (post.photo && post.photo.length > 0 && ![post.photo isEqualToString:@"/photos/original/missing.png"]) {
        image = 275.0;
    }else{
        image = 0.0;
    }
    
    if(image>=62.0)
    {
        image = image-header;
    }
    
    CGFloat content = [self heightWithPost:post];
    CGFloat footer = 50.0;
    
    CGFloat height = header + image + content + footer;

//    NSLog(@"Height for row %i is %f", indexPath.row, height);
    
    
    return height;
    
}

-(CGFloat)heightForImage:(UIImage*)image inImageViewAspectFit:(UIImageView*)imageView
{
    if (!image) {
        return 0;
    }
    
    float imageRatio = image.size.width / image.size.height;
    
    float viewRatio = imageView.frame.size.width / imageView.frame.size.height;
    
    if(imageRatio < viewRatio)
    {
        return  imageView.frame.size.height;
    }
    else
    {
        float scale = imageView.frame.size.width / image.size.width;
        
        float height = scale * image.size.height;
        
        return  height;
    }
}

- (CGFloat) heightWithPost:(CLYPost *)post
{
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13.5f]];
    [calculationView setText:post.content];
    CGSize expectedSize = [calculationView sizeThatFits:CGSizeMake(290.0f, MAXFLOAT)];
    
    return ceil(expectedSize.height) + 7.0f * 2; // 7 px padding top & bottom
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return;
    }
    
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *postStoryBoard = [UIStoryboard storyboardWithName:@"Post" bundle:nil];
    CLYPostViewController *vc = [postStoryBoard instantiateInitialViewController];
    CLYPost *post = (CLYPost *)[self.posts objectAtIndex:indexPath.row];
    vc.postId = post.id;
    [self.navigationController pushViewController:vc animated:YES];
    
}




-(void)openUser:(NSNotification*)sender
{
//    NSLog(@"UserViewController openUser: %@",sender.object);
    
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"User" bundle:nil];
    CLYUserViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYUserViewController"];
    vc.userId = sender.object;
    
    [self.navigationController setViewControllers:@[vc] animated:YES];
    
}



@end
