//
//  CLYCommunityViewController.h
//  Communly
//
//  Created by Chris Allwein on 4/4/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYCommunityViewController : UITableViewController

@property(nonatomic, strong)  NSString *communityId;

@end
