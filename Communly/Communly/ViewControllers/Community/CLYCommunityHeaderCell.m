//
//  CLYCommunityHeaderCell.m
//  Communly
//
//  Created by Chris Allwein on 4/4/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCommunityHeaderCell.h"
#import "CLYApplicationConstants.h"
#import "CLYCommunityWebService.h"

@implementation CLYCommunityHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.joinButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    [self.joinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.joinButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    [self.joinButton setTitle:@"+ Join Community" forState:UIControlStateNormal];

    self.memberCountLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    self.typeLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];

    self.communityNameLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];
    self.communityTypeLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.bannerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];

    self.communityAvatarImageView.layer.cornerRadius = 3.0;
    self.communityAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.communityAvatarImageView.layer.borderWidth = 1.0;
    self.communityAvatarImageView.clipsToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)joinButtonTapped:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"+ Join Community"]) {
        [[CLYCommunityWebService sharedService] joinCommunityWithId:self.communityId];
        [button setTitle:@"- Unjoin Community" forState:UIControlStateNormal];
    }else{
        [[CLYCommunityWebService sharedService] unjoinCommunityWithId:self.communityId];
        [button setTitle:@"+ Join Community" forState:UIControlStateNormal];
    }

    
}
@end
