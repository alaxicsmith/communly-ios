//
//  CLYCommunityHeaderCell.h
//  Communly
//
//  Created by Chris Allwein on 4/4/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYCommunityHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *communityBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *communityAvatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
- (IBAction)joinButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UILabel *memberCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *membersImageView;
@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;
@property (weak, nonatomic) IBOutlet UILabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *communityTypeLabel;
@property (nonatomic, strong) NSString *communityId;

@end
