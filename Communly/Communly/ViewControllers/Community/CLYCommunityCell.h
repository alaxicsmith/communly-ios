//
//  CLYCommunityCell.h
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYCommunityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIImageView *communityImageView;
@property (weak, nonatomic) IBOutlet UILabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *memberCountImageView;

@property (strong, nonatomic) NSString *communityId;

- (IBAction)joinButtonTapped:(id)sender;


@end
