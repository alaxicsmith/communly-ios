//
//  CLYPostHeaderCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"

@interface CLYPostHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeSinceLabel;
@property (weak, nonatomic) IBOutlet UIButton *userButton;
- (IBAction)userButtonTapped:(UIButton *)sender;

@property (strong, nonatomic) NSString *userId;

- (void) setupWithPost:(CLYPost *)post;

+ (CGFloat) heightWithPost:(CLYPost *)post;

@end
