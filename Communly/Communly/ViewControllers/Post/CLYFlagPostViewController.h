//
//  CLYFlagPostController.h
//  Communly
//
//  Created by Chris Allwein on 7/23/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYFlagPostViewController : UIViewController
@property (nonatomic,strong) NSString *postId;
@end
