//
//  CLYPostViewController.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYViewController.h"

@interface CLYPostViewController : UITableViewController <UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSString *postId;

@end
