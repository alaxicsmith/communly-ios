//
//  CLYFlagPostController.m
//  Communly
//
//  Created by Chris Allwein on 7/23/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYFlagPostViewController.h"
#import "CLYApplicationConstants.h"
#import "CLYPostsWebService.h"

@interface CLYFlagPostViewController () <UIActionSheetDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIButton *selectReasonButton;
- (IBAction)selectReasonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (strong, nonatomic) NSArray *reasons;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

@implementation CLYFlagPostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Report a Problem";
    [self.navBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    [self.navBar setBarTintColor:[UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0]];
    self.navBar.translucent = NO;
    _reasons = @[@"It's obscene.",@"It's objectionable/offensive.",@"It's abusive.",@"Other"];
    self.commentTextView.delegate = self;
    self.reasonLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:15.0];
    
    self.selectReasonButton.titleLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.selectReasonButton.titleLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.selectReasonButton.tintColor = [CLYApplicationConstants applicationBackgroundColor];
    self.selectReasonButton.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    self.selectReasonButton.layer.cornerRadius = 3.0;
    self.selectReasonButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    self.submitButton.titleLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.submitButton.tintColor = [UIColor whiteColor];
    self.submitButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    self.submitButton.layer.cornerRadius = 3.0;
    
    self.commentTextView.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    self.commentTextView.tintColor = [CLYApplicationConstants applicationBackgroundColor];
    self.commentTextView.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.commentTextView.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    [self.commentTextView setTextContainerInset:UIEdgeInsetsMake(5, 5, 0, 0) ];
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
//    [self.navBar setRightBarButtonItem:cancelButton animated:YES];
    
        UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"Report a Problem"];
    item.leftBarButtonItem = cancelButton;
    item.hidesBackButton = YES;
    [self.navBar pushNavigationItem:item animated:NO];
  }

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submitButtonTapped:(id)sender {
    
    [[CLYPostsWebService sharedService] reportPostAsFlagged:self.postId user:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] reason:self.selectReasonButton.titleLabel.text comment:self.commentTextView.text];
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView  alloc] initWithTitle:@"Thank You" message:@"We will review your problem report and take appropriate action." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        
    }];
}

-(void) displayCommunityActionSheet
{
    UIActionSheet* actionSheet = [[UIActionSheet alloc] init];
    actionSheet.title = @"I have a problem with this post because...";
    
    actionSheet.delegate = self;
    for (NSString *reason in self.reasons) {
        [actionSheet addButtonWithTitle:reason];
    }

    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    actionSheet.tintColor = [CLYApplicationConstants applicationBackgroundColor];
    [actionSheet showInView:self.view];
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex < [self.reasons count]) {
        [self.selectReasonButton setTitle:[self.reasons objectAtIndex:buttonIndex] forState:UIControlStateNormal];
        [self.view layoutIfNeeded];
    }
    
}

- (IBAction)selectReasonTapped:(id)sender {
    [self displayCommunityActionSheet];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	[self.commentTextView becomeFirstResponder];
	return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Add additional detail here..."]) {
        textView.text = @"";
    }
    
}

@end
