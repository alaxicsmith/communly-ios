//
//  CLYPostHeaderCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostHeaderCell.h"
#import "CLYHelperFunctions.h"
#import "CLYUser.h"

#import "UIImageView+AFNetworking.h"

@implementation CLYPostHeaderCell

- (void)awakeFromNib
{
    // Initialization code
    _userNameLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0f];
    _timeSinceLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.0f];
    _userIconImageView.layer.cornerRadius = 3.0;
    _userIconImageView.layer.masksToBounds = YES;

}

- (IBAction)userButtonTapped:(UIButton *)sender {
       [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_USER" object:self.userId];
}

- (void) setupWithPost:(CLYPost *)post
{
    [_userIconImageView setImageWithURL:[NSURL URLWithString:post.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
    _userNameLabel.text = post.user.name;
    _userId = post.user.id;
    
    _timeSinceLabel.text = [CLYHelperFunctions timePostedFormatted:post.created_at];
}

+ (CGFloat) heightWithPost:(CLYPost *)post
{
    return 61.0f;
}

@end
