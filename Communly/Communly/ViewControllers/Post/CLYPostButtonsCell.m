//
//  CLYPostButtonsCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostButtonsCell.h"
#import "CLYPostsWebService.h"

@implementation CLYPostButtonsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)favesButtonTapped:(id)sender {
//    NSLog(@"FAVES BUTTON TAPPED");
    [[CLYPostsWebService sharedService] markPostAsFavorite:self.post.id forUser:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    self.favesButton.favesCount++;
    self.favesButton.enabled = NO;
}

- (IBAction)commentsButtonTapped:(id)sender {
//        NSLog(@"COMMENTS BUTTON TAPPED");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CREATE_COMMENT" object:self.post.id];
}

- (IBAction)reportButtonTapped:(id)sender {
//    NSLog(@"Report BUTTON TAPPED");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REPORT_POST" object:self.post.id];

    
}

- (void) setupWithPost:(CLYPost *)post
{
    _post = post;
    _favesButton.favesCount =[post.favesCount intValue];
    
}

+ (CGFloat) heightWithPost:(CLYPost *)post
{
    return 44.0f;
}

@end
