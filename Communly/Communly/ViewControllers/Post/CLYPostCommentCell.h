//
//  CLYPostCommentCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"

@interface CLYPostCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeSinceLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *userButton;

@property (strong, nonatomic) NSString *userId;

- (IBAction)userButtonTapped:(id)sender;

+ (CGFloat) heightWithPost:(CLYPost *)post commentNumber:(NSNumber *)commentNumber;

- (void) setupWithPost:(CLYPost *)post commentNumber:(NSNumber *)commentNumber;

@end
