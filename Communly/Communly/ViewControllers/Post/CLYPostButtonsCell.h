//
//  CLYPostButtonsCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"
#import "CLYFavesButton.h"

@interface CLYPostButtonsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CLYFavesButton *favesButton;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@property (weak, nonatomic) CLYPost *post;

- (IBAction)favesButtonTapped:(id)sender;
- (IBAction)commentsButtonTapped:(id)sender;
- (IBAction)reportButtonTapped:(id)sender;


- (void) setupWithPost:(CLYPost *)post;

+ (CGFloat) heightWithPost:(CLYPost *)post;

@end
