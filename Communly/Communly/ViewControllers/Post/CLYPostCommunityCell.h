//
//  CLYPostCommunityCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"

@interface CLYPostCommunityCell : UITableViewCell

@property (strong, nonatomic) NSString *communityId;
@property (weak, nonatomic) IBOutlet UIButton *communityButton;
- (IBAction)openCommunity:(UIButton *)sender;

- (void) setupWithPost:(CLYPost *)post;

+ (CGFloat) heightWithPost:(CLYPost *)post;

@end
