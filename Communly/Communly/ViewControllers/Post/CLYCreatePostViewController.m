//
//  CLYPostViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCreatePostViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CLYPost.h"
#import "CLYPostsWebService.h"
#import "CLYUserWebService.h"
#import "CLYCommunityWebService.h"
#import "CLYApplicationConstants.h"
#import "CLYCommunity.h"
#import "UIImage+fixOrientation.h"

@interface CLYCreatePostViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UILabel *communityLabel;
@property (weak, nonatomic) IBOutlet NSString *communityId;

@property (weak, nonatomic) IBOutlet UITextView *postTextView;
@property (weak, nonatomic) IBOutlet UIButton *addCommunityButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addImageButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addSomethingButton;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
- (IBAction)addImageTapped:(id)sender;
- (IBAction)addSomethingTapped:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarVerticalPositionConstraint;

@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *errorButton;
- (IBAction)errorButtonTapped:(id)sender;

- (IBAction)addCommunityButtonTapped:(id)sender;
@property (nonatomic,strong) NSArray *communities;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageConstraint;

@end

@implementation CLYCreatePostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor =[UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cancelPost"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped:)];
    cancelButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:cancelButton animated:YES];

    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"savePost"] style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped:)];
    saveButton.imageInsets = UIEdgeInsetsMake(0, -15, 0,15);
    [self.navigationItem setRightBarButtonItem:saveButton animated:YES];

    self.title = @"Create Post";
    
    self.toolbar.tintColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    self.addImageButton.width = 160;
    self.addSomethingButton.width = 160;

    
    self.userImageView.image = [self roundCorneredImage:self.userImageView.image radius:5.0];
    self.userLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];
    self.userLabel.textColor = [UIColor colorWithRed:77.0/255.0 green:77.0/255.0 blue:77.0/255.0 alpha:1.0];
    
    self.addCommunityButton.layer.borderColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0].CGColor;
    self.addCommunityButton.layer.borderWidth = 1.0;
    self.addCommunityButton.layer.cornerRadius = 3.0;
    self.addCommunityButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    [self.addCommunityButton setTitleColor:[UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    self.communityLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];


    self.postTextView.textColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    self.postTextView.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];
    self.postTextView.tintColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    self.postTextView.contentInset = UIEdgeInsetsZero;
    self.postTextView.layoutManager.allowsNonContiguousLayout = NO;
    
    self.errorLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
    self.errorView.backgroundColor = [CLYApplicationConstants redButtonColor];
    
    self.userNameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    self.userLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userfullname"];
    [self.userImageView setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
    
    [self registerForKeyboardNotifications];
    self.communities = [NSArray array];
    
    [[CLYCommunityWebService sharedService] getCommunitiesWithUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] completionHandler:^(NSArray *communities) {
        self.communities = communities;
        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (UIImage*) roundCorneredImage: (UIImage*) orig radius:(CGFloat) r {
    UIGraphicsBeginImageContextWithOptions(orig.size, NO, 0);
    [[UIBezierPath bezierPathWithRoundedRect:(CGRect){CGPointZero, orig.size}
                                cornerRadius:r] addClip];
    [orig drawInRect:(CGRect){CGPointZero, orig.size}];
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelTapped:(id)sender {
//    NSLog(@"Cancel Tapped");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)saveTapped:(id)sender {
//    NSLog(@"Save Tapped");
    
    if (!self.communityId || [self.communityId isEqualToString:@""]) {
        self.errorView.hidden = NO;
        
        return;
    }
    
    if ([self.postTextView.text isEqualToString:@"Write Something..."]) {
        
       return;
    }
    
    
    
    [[CLYPostsWebService sharedService] createPostWithContent:self.postTextView.text withImage:self.postImageView.image forCommunityWithId:self.communityId byUserWithId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addImageTapped:(id)sender {
//    NSLog(@"AddImageTapped");
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [[info valueForKey:UIImagePickerControllerOriginalImage]fixOrientation];
//    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    self.postImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.postImageView.image = image;
    
    self.imageConstraint.constant = 170.0;
    [self.view layoutIfNeeded];
//    [[FTDatasource sharedDatasource] uploadImage:image forContainer:@"menu" withCompletion:^(NSString *string) {
//        self.menuUrl = string;
//    }];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)addSomethingTapped:(id)sender {
//        NSLog(@"addSomethingTapped");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    CGRect frame = self.textView.frame;
//    frame.size.height = frame.size.height - kbSize.height;
//    self.textView.frame = frame;

    if ([self.postTextView.text isEqualToString:@"Write Something..."]) {
        self.postTextView.text = @"";
    }
    
    NSDictionary *info = [aNotification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];

    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];

    CGRect finalKeyboardFrame = [self.view convertRect:keyboardFrame fromView:self.view.window];

    int kbHeight = finalKeyboardFrame.size.height;

    int height = kbHeight;// + self.bottomConstraint.constant;

    self.toolbarVerticalPositionConstraint.constant = height; //self.toolbarVerticalPositionConstraint.constant + height;

    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark UITextView Delegate
-(void)textViewDidChange:(UITextView *)textView
{
    CGSize newsize = [self calcTextSize];
//    NSLog(@"new width: %f   new height: %f", newsize.width, newsize.height);
    if (newsize.height > 30.0) {
        self.postTextViewHeightConstraint.constant = newsize.height + 2;
                     [self.view layoutIfNeeded];
    }else{
        self.postTextViewHeightConstraint.constant = 30.0;
                     [self.view layoutIfNeeded];
    }

    
    CGRect line = [textView caretRectForPosition:
                   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( textView.contentOffset.y + textView.bounds.size.height
       - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

- (CGSize)calcTextSize
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //To get multi-line
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.lineSpacing = 1;
    
    //Create the font using the values set by the user
    UIFont *font =  self.postTextView.font ;
    
    //Create our textAttributes dictionary that we'll use when drawing to the graphics context
    NSMutableDictionary *textAttributes = [NSMutableDictionary dictionary];
    
    //Font Name and size
    [textAttributes setObject:font forKey:NSFontAttributeName];
    
    //Line break mode and alignment
    [textAttributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    
    NSString *text = [self.postTextView.text stringByAppendingString:@"A"];
    //Calculate the size that the text will take up, given our options.  We use the full screen size for the bounds
    CGRect textRect = [text boundingRectWithSize:[[UIScreen mainScreen] bounds].size
                                         options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine
                                      attributes:textAttributes
                                         context:nil];
    
    //iOS7 uses fractional size values.  So we needed to ceil it to make sure we have enough room for display.
    textRect.size.height = ceil(textRect.size.height);
    textRect.size.width = ceil(textRect.size.width);

    return textRect.size;
    
}

- (IBAction)errorButtonTapped:(id)sender {
     self.errorView.hidden = YES;
}

- (IBAction)addCommunityButtonTapped:(id)sender {
    [self displayCommunityActionSheet];
}

-(void) displayCommunityActionSheet
{
    UIActionSheet* actionSheet = [[UIActionSheet alloc] init];
    actionSheet.title = @"Communities";
    
    actionSheet.delegate = self;
    
    for (CLYCommunity *community in self.communities) {
        [actionSheet addButtonWithTitle:community.name];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    [actionSheet showInView:self.view];
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex < [self.communities count]) {
        [self.addCommunityButton setTitle:@"Change Community" forState:UIControlStateNormal];
        CLYCommunity *community =[self.communities objectAtIndex:buttonIndex];
        self.communityLabel.text = [NSString stringWithFormat:@"%@ ",community.name];
        self.communityId = community.id;
        [self.view layoutIfNeeded];
    }
    
}

@end
