//
//  CLYPostDetailCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostDetailCell.h"

@implementation CLYPostDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    _postTextView.font = [UIFont fontWithName:@"ProximaNova-Light" size:12.0f];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void) setupWithPost:(CLYPost *)post
{
    _postTextView.text = post.content;
}

#pragma mark - class methods

+ (CGFloat) heightWithPost:(CLYPost *)post
{
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13.5f]];
    [calculationView setText:post.content];
    CGSize expectedSize = [calculationView sizeThatFits:CGSizeMake(290.0f, MAXFLOAT)];
   
    return ceil(expectedSize.height) + 7.0f * 2; // 7 px padding top & bottom
}

@end
