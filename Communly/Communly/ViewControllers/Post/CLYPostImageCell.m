//
//  CLYPostImageCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostImageCell.h"
#import "UIImageView+AFNetworking.h"

@implementation CLYPostImageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)imageButtonTapped:(UIButton *)sender {
    NSLog(@"imageButton Tapped!");
}

- (void)setupWithPost: (CLYPost *)post
{
    [_postImageView setImageWithURL:[NSURL URLWithString:post.photo] placeholderImage:nil];
}

+ (CGFloat) heightWithPost:(CLYPost *)post
{
    // TODO: Has to be a better way here
    if (!post.photo || post.photo.length == 0 || [post.photo isEqualToString:@"/photos/original/missing.png"]) return 0.0f;
//#warning Need to calculate height of image
    return 280.0f;
}
@end
