//
//  CLYPostCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostCommunityCell.h"
#import "CLYCommunity.h"

@implementation CLYPostCommunityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)openCommunity:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_COMMUNITY" object:self.communityId];
}

- (void) setupWithPost:(CLYPost *)post
{
    NSString *communityString = [NSString stringWithFormat:@"community: %@", post.community.name];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:communityString];
    
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5] range:NSMakeRange(0, attString.length)];
    
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0] range:NSMakeRange(0, attString.length)];

    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0] range:NSMakeRange(10, attString.length-10)];
    
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init ];
    pStyle.alignment = NSTextAlignmentLeft;
    [attString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, attString.length)];
    _communityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_communityButton setAttributedTitle:attString forState:UIControlStateNormal];

    _communityId = post.community.id;
}

+ (CGFloat) heightWithPost:(CLYPost *)post
{
    return 30.0f;
}

@end
