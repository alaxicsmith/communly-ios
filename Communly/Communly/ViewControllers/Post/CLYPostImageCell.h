//
//  CLYPostImageCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"

@interface CLYPostImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
- (IBAction)imageButtonTapped:(UIButton *)sender;

- (void)setupWithPost: (CLYPost *)post;

+ (CGFloat) heightWithPost:(CLYPost *)post;

@end
