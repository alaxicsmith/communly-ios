//
//  CLYPostCommentCell.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostCommentCell.h"
#import "CLYComment.h"
#import "CLYUser.h"
#import "CLYHelperFunctions.h"

#import "UIImageView+AFNetworking.h"


@implementation CLYPostCommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    _commentTextView.font = [UIFont fontWithName:@"ProximaNova-Light" size:12.0f];
    _userNameLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0f];
    _timeSinceLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.0f];
    _userIconImageView.layer.cornerRadius = 3.0;
    _userIconImageView.layer.masksToBounds = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void) setupWithPost:(CLYPost *)post commentNumber:(NSNumber *)commentNumber
{
    CLYComment *comment = (CLYComment *)post.comments[[commentNumber longValue]];
    [self.userIconImageView setImageWithURL:[NSURL URLWithString:comment.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
//    NSLog(@"Comment   id: %@", comment.id);
//    NSLog(@"Comment from: %@", comment.user.name);
//    NSLog(@"Comment text: %@", comment.content);
//    NSLog(@"Comment created at: %@", comment.created_at.description);
    
    _userId = comment.user.id;
    self.userNameLabel.text = comment.user.name;
    self.timeSinceLabel.text = [CLYHelperFunctions timePostedFormatted:comment.created_at];
    self.commentTextView.text = comment.content;
}

#pragma mark - class methods

- (IBAction)userButtonTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_USER" object:self.userId];
    
}

+ (CGFloat) heightWithPost:(CLYPost *)post commentNumber:(NSNumber *)commentNumber
{
    if(!post || !commentNumber) return 0.0f;
    
//    NSLog(@"Comment count: %lu - comment #: %@", (unsigned long)post.comments.count, commentNumber);
//    NSLog(@"Comment text: %@", ((CLYComment *)post.comments[[commentNumber longValue]]).content);
    
    if ([commentNumber longValue] >= post.comments.count || commentNumber < 0) return 0.0f; // Should never happen

    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setFont:[UIFont fontWithName:@"ProximaNova-Light" size:12.0f]];
    [calculationView setText:((CLYComment *)post.comments[[commentNumber longValue]]).content];
    CGSize expectedSize = [calculationView sizeThatFits:CGSizeMake(274.0f, MAXFLOAT)];
    CGFloat expectedHeight = ceil(expectedSize.height) + 37.0f; // 30pt padding top, 7pt padding bottom
    
//    NSLog(@"Calc expected size: %.2f", expectedHeight);
    return expectedHeight;
}
@end
