//
//  CLYPostViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostViewController.h"
#import "CLYPostsWebService.h"
#import "CLYPost.h"

#import "CLYPostHeaderCell.h"
#import "CLYPostDetailCell.h"
#import "CLYPostImageCell.h"
#import "CLYPostCommunityCell.h"
#import "CLYPostButtonsCell.h"
#import "CLYPostCommentCell.h"

#import "CLYApplicationConstants.h"
#import "CLYCommentTextField.h"

#import "CLYFlagPostViewController.h"

#define kCLYPostHeaderCellIndexPathRow 0
#define kCLYPostDetailCellIndexPathRow 2
#define kCLYPostImageCellIndexPathRow 1
#define kCLYPostCommunityCellIndexPathRow 3
#define kCLYPostButtonsCellIndexPathRow 4
#define kCLYPostCommentCellIndexPathRow 5

@interface CLYPostViewController ()
{
    CLYPost *_post;
    
}

@property (nonatomic,strong) UITextField *hiddenCommentTextField;
@property (nonatomic,strong) UIView *commentAccessoryView;
@property (nonatomic,strong) CLYCommentTextField *commentTextField;
@property (nonatomic,strong) UIButton *commentButton;
           

@end

@implementation CLYPostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.title = @"Post";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];
    
    self.tableView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createComment:) name:@"CREATE_COMMENT" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flagPost:) name:@"REPORT_POST" object:nil];

    
    [self registerForKeyboardNotifications];
    
//    NSLog(@"PostId: %@",self.postId);
    self.tableView.hidden = YES;
    [[CLYPostsWebService sharedService] getPostWithId:self.postId completionHandler:^(CLYPost *post)
    {
        _post = post;
//        NSLog(@"Got Post in viewDidLoad: %@", _post);
        [self.tableView reloadData];
        self.tableView.hidden = NO;
    }];
    
}

-(void)flagPost:(id)sender
{
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"Flag" bundle:nil];
    CLYFlagPostViewController *vc = [post instantiateViewControllerWithIdentifier:@"CLYFlagPostViewController"];
    vc.postId = self.postId;
    vc.modalPresentationStyle = UIModalTransitionStyleCoverVertical;
    [self.navigationController presentViewController:vc animated:YES completion:^{ }];
}

-(void) createComment:(id)sender
{
//    NSLog(@"Showing comment entry field");
    _commentAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _commentAccessoryView.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];

    _commentTextField = [[CLYCommentTextField alloc] initWithFrame:CGRectMake(8, 8, 223, 28)];
    _commentTextField.delegate = self;


    _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _commentButton.frame = CGRectMake(239, 8, 73, 28);
    NSAttributedString *attrButtonTitle = [[NSAttributedString alloc] initWithString:@"Comment" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-SemiBold" size:12.0], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [_commentButton setAttributedTitle:attrButtonTitle forState:UIControlStateNormal];
    _commentButton.layer.cornerRadius = 3.0;
    _commentButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    [_commentButton addTarget:self action:@selector(saveComment) forControlEvents:UIControlEventTouchUpInside];

    [_commentAccessoryView addSubview:_commentTextField];
    [_commentAccessoryView addSubview:_commentButton];
    
    _hiddenCommentTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _hiddenCommentTextField.delegate = self;
    [self.view.window addSubview:_hiddenCommentTextField];
    [_hiddenCommentTextField becomeFirstResponder];
    
}



-(void)saveComment
{
//    NSLog(@"Saving Comment: %@", _commentTextField.text);
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];

    [[CLYPostsWebService sharedService] commentOnPost:self.postId user:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] content:_commentTextField.text completionHandler:^{
        [[CLYPostsWebService sharedService] getPostWithId:self.postId completionHandler:^(CLYPost *post)
         {
             _post = post;
//             NSLog(@"Got Post in viewDidLoad: %@", _post);
             [self.tableView reloadData];
         }];
        
    }];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self saveComment];
    return YES;
}

#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
     [_commentTextField becomeFirstResponder];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
  

}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    // Now add the view as an input accessory view to the selected textfield.
    if (textField != _commentTextField) {
        [textField setInputAccessoryView:_commentAccessoryView];
       
    }
   
}


-(void) back
{
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)return 5 + _post.comments.count;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_post == nil){
//        NSLog(@"No post yet - trying to find height of cells!");
        return 0.0f;
    }
//    NSLog(@"Have post - find height of cell at row %li", (long)indexPath.row);
    switch (indexPath.row) {
        case kCLYPostHeaderCellIndexPathRow:
            return [CLYPostHeaderCell heightWithPost:_post];
            break;
        case kCLYPostDetailCellIndexPathRow:
            return [CLYPostDetailCell heightWithPost:_post];
            break;
        case kCLYPostImageCellIndexPathRow:
            return [CLYPostImageCell heightWithPost:_post];
            break;
        case kCLYPostButtonsCellIndexPathRow:
            return [CLYPostButtonsCell heightWithPost:_post];
            break;
        case kCLYPostCommunityCellIndexPathRow:
            return [CLYPostCommunityCell heightWithPost:_post];
            break;
        default:
//            NSLog(@"Checking the height for a comment cell at indexPath %@\nShould be comment # %@", indexPath, [NSNumber numberWithLong:(indexPath.row - kCLYPostCommentCellIndexPathRow)]);
            return [CLYPostCommentCell heightWithPost:_post commentNumber:[NSNumber numberWithLong:(indexPath.row - kCLYPostCommentCellIndexPathRow)]];
            break;
    }
//    NSLog(@"Trying to find the height of a cell that's not there!!!");
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"cellForRowAtIndexPath - section: %li row: %li", indexPath.section, indexPath.row);
    UITableViewCell *cell;
    //if (_post == nil) return nil; // TODO - Loading?
    
    if (indexPath.row == kCLYPostHeaderCellIndexPathRow)
    {
        CLYPostHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostHeaderCell"];
        [headerCell setupWithPost:_post];
        cell = headerCell;
    }
    else if (indexPath.row == kCLYPostDetailCellIndexPathRow)
    {
        CLYPostDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostDetailCell"];
        [detailCell setupWithPost:_post];
        cell = detailCell;
    }
    else if (indexPath.row == kCLYPostImageCellIndexPathRow)
    {
        CLYPostImageCell *imageCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostImageCell"];
        [imageCell setupWithPost:_post];
        cell = imageCell;
    }
    else if (indexPath.row == kCLYPostCommunityCellIndexPathRow)
    {
        CLYPostCommunityCell *communityCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostCommunityCell"];
        [communityCell setupWithPost:_post];
        cell = communityCell;
    }
    else if (indexPath.row == kCLYPostButtonsCellIndexPathRow)
    {
        CLYPostButtonsCell *buttonsCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostButtonsCell"];
        [buttonsCell setupWithPost:_post];
        cell = buttonsCell;
    }
    else // Everything else is a comment cell
    {
        CLYPostCommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"CLYPostCommentCell"];
        [commentCell setupWithPost:_post commentNumber:[NSNumber numberWithLong:(indexPath.row - kCLYPostCommentCellIndexPathRow)]];
        cell = commentCell;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
