//
//  CLYPostDetailCell.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYPost.h"

@interface CLYPostDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *postTextView;

+ (CGFloat) heightWithPost:(CLYPost *)post;

- (void) setupWithPost:(CLYPost *)post;

@end
