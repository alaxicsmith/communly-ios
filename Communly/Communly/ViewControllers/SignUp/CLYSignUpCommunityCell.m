//
//  CLYSignUpCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpCommunityCell.h"
#import "CLYApplicationConstants.h"
#import "CLYCommunityWebService.h"
@implementation CLYSignUpCommunityCell
//
//  CLYCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

- (void)awakeFromNib
{
    // Initialization code
    self.contentView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.backgroundImageView.layer.cornerRadius = 3.0;
    self.backgroundImageView.clipsToBounds = YES;
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.avatarImageView.layer.cornerRadius = 3.0;
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.avatarImageView.layer.borderWidth = 2.0;
    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    self.joinButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.joinButton.layer.cornerRadius = 3.0;
    //    self.joinButton.layer.borderWidth = 1.0;
    self.joinButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    //    self.joinButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.communityNameLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:16.0];
    self.communityMemberLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    self.communityTagLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (IBAction)joinButtonTapped:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"+ Join"]) {
        [[CLYCommunityWebService sharedService] joinCommunityWithId:self.communityId];
        [button setTitle:@"" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"joinCheckmark"] forState:UIControlStateNormal];
         self.joinButton.backgroundColor = [UIColor clearColor];
    }else{
        [[CLYCommunityWebService sharedService] unjoinCommunityWithId:self.communityId];
         self.joinButton.backgroundColor = [CLYApplicationConstants redButtonColor];
        [button setImage:nil forState:UIControlStateNormal];
        [button setTitle:@"+ Join" forState:UIControlStateNormal];

    }
    
}

-(void)prepareForReuse
{
    self.communityNameLabel.text = @"";
    self.backgroundImageView.image = nil;
    [self.joinButton setTitle:@"+ Join" forState:UIControlStateNormal];
}

@end
