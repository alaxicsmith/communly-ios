//
//  CLYSignUpFindFriendsViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpFindFriendsViewController.h"
#import "CLYFindFriendCell.h"
#import "CLYFindFriendSectionCell.h"
#import "CLYApplicationConstants.h"
#import "CLYUser.h"
#import "UIImageView+AFNetworking.h"
#import "CLYUserWebService.h"

@interface CLYSignUpFindFriendsViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *users;

@end

@implementation CLYSignUpFindFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Find Friends";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    self.users = [NSArray array];

    [self.toolbar setBackgroundImage:[[UIImage alloc] init] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];

    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];
    self.tableView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.toolbar.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    [[CLYUserWebService sharedService] getFeaturedUsersWithCompletionHandler:^(NSArray *users) {
        self.users = users;
        [self.tableView reloadData];
    }];

}
-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)completeSignup:(id)sender {
    UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Stream" bundle:nil];
    UIViewController *vc = [stream instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;

//    if(section==2)
//    {
//        return 0;
//    }
//
//    return 44.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==2)
    {
        return [self.users count];
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CLYFindFriendSectionCell *cell = (CLYFindFriendSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"friendSectionCell"];
    if (cell ==nil ) {
        cell = [[CLYFindFriendSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"friendSectionCell"];
    }
    
    switch (section) {
        case 0:
            cell.sectionNameLabel.text = @"Facebook";
            cell.sectionImageView.image = [UIImage imageNamed:@"FacebookLogo"];
            break;
            
        case 1:
            cell.sectionNameLabel.text = @"Twitter";
            cell.sectionImageView.image = [UIImage imageNamed:@"TwitterLogo"];
            break;
            
        case 2:
            return nil;
            break;
            
        default:
            break;
    }

    return cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYFindFriendCell *cell = (CLYFindFriendCell*)[tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    if (cell ==nil ) {
        cell = [[CLYFindFriendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"friendCell"];
    }
    
    CLYUser *user = [self.users objectAtIndex:indexPath.row];
    
    [cell.friendImageView setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
    cell.friendNameLabel.text = user.name;
    cell.friendLocationLabel.text = user.location;
    cell.userId = user.id;
    
    return cell;
}

@end
