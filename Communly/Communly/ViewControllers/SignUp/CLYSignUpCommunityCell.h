//
//  CLYSignUpCommunityCell.h
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYLocationLabel.h"

@interface CLYSignUpCommunityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UILabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *communityMemberLabel;
@property (weak, nonatomic) IBOutlet UILabel *communityTagLabel;

@property (strong, nonatomic) NSString *communityId;

- (IBAction)joinButtonTapped:(id)sender;


@end
