//
//  CLYFindFriendCell.h
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYFindFriendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *friendImageView;
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendLocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
- (IBAction)followButtonTapped:(id)sender;
@property (strong, nonatomic) NSString *userId;

@end
