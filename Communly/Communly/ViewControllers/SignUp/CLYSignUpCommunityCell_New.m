//
//  CLYSignUpCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpCommunityCell_New.h"
#import "CLYApplicationConstants.h"
#import "CLYCommunityWebService.h"
@implementation CLYSignUpCommunityCell_New
//
//  CLYCommunityCell.m
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

- (void)awakeFromNib
{
    // Initialization code
    self.contentView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.backgroundImageView.layer.cornerRadius = 3.0;
    self.backgroundImageView.clipsToBounds = YES;
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.joinButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:10];
    self.joinButton.layer.cornerRadius = 3.0;
    //    self.joinButton.layer.borderWidth = 1.0;
    self.joinButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    //    self.joinButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.communityNameLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:20.0];
    self.communityMemberLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:13.5];
    self.communityTagLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:13.5];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (IBAction)joinButtonTapped:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"+ Join"]) {
        [[CLYCommunityWebService sharedService] joinCommunityWithId:self.communityId];
        [button setTitle:@"- Unjoin" forState:UIControlStateNormal];
    }else{
        [[CLYCommunityWebService sharedService] unjoinCommunityWithId:self.communityId];
        [button setTitle:@"+ Join" forState:UIControlStateNormal];
    }
    
}

-(void)prepareForReuse
{
    self.communityNameLabel.text = @"";
    self.backgroundImageView.image = nil;
    [self.joinButton setTitle:@"+ Join" forState:UIControlStateNormal];
}

@end
