//
//  CLYSignUpAccountViewController.h
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYSignUpAccountViewController : UIViewController <UITextFieldDelegate>

@end
