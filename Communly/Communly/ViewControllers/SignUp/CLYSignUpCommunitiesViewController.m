//
//  CLYSignUpCommunitiesViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpCommunitiesViewController.h"
#import "CLYSignUpCommunityCell.h"
#import "CLYCommunity.h"
#import "CLYApplicationConstants.h"
#import "CLYCommunityWebService.h"
#import "UIImageView+AFNetworking.h"

@interface CLYSignUpCommunitiesViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *communities;

@end

@implementation UIImage (AverageColor)

- (UIColor *)averageColor {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGRect myImageArea = CGRectMake(74, 113, 150, 152);//newImage
    CGImageRef mySubimage  = CGImageCreateWithImageInRect(self.CGImage, myImageArea);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), mySubimage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

@end


@implementation CLYSignUpCommunitiesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.communities = [NSArray array];

    self.title = @"Join Communities";

    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];
    
    self.tableView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.toolbar.backgroundColor = [CLYApplicationConstants applicationTransparentBackgroundColor];

    [[CLYCommunityWebService sharedService] getFeaturedCommunitiesWithCompletionHandler:^(NSArray *communities) {
        self.communities = communities;
        [self.tableView reloadData];
    }];
    
}

-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.communities count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYSignUpCommunityCell *cell = (CLYSignUpCommunityCell*)[tableView dequeueReusableCellWithIdentifier:@"CLYSignUpCommunityCell"];
    
    CLYCommunity *community = [self.communities objectAtIndex:indexPath.row];
    
    cell.communityNameLabel.text = community.name;
    cell.communityMemberLabel.text = [NSString stringWithFormat:@"%@ Members", community.communityMemberCount ];
    cell.communityId = community.id;
    cell.communityNameLabel.textColor = [UIColor whiteColor];
    cell.communityMemberLabel.textColor = [UIColor whiteColor];

    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:community.background]];
    [cell.backgroundImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        cell.backgroundImageView.image = image;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
    }];
    
//    if (self.user.location) {
//        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//        attachment.image = [UIImage imageNamed:@"communityTag"];
//        attachment.bounds = CGRectMake(0, -6, 21, 21);
//        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
//        
//        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:community.category];
//        [attachmentString appendAttributedString:myString];
    
        cell.communityTagLabel.text = community.category;
//    }

    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 190;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 44.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"footer"];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}
@end
