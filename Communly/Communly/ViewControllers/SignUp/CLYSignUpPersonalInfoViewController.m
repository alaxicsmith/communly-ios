//
//  CLYSignUpPersonalInfoViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpPersonalInfoViewController.h"
#import "CLYApplicationConstants.h"
#import "CLYUserWebService.h"
#import "CLYUser.h"
#import "CLYTextField.h"
#import "UIImage+fixOrientation.h"

@interface CLYSignUpPersonalInfoViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet CLYTextField *locationTextField;
@property (weak, nonatomic) IBOutlet UITextView *bioTextView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewConstraint;

@property (strong, nonatomic)  UIImage *userImage;

- (IBAction)addPictureTapped:(id)sender;
- (IBAction)nextButtonTapped:(id)sender;

@end

@implementation CLYSignUpPersonalInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self registerForKeyboardNotifications];

    // Do any additional setup after loading the view.
    self.title = @"Add Info";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    [self.toolbar setBackgroundImage:[[UIImage alloc] init] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];
    self.view.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.toolbar.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    [self.locationTextField setLeftViewMode:UITextFieldViewModeAlways];

    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LocationMarker"]];

    self.locationTextField.leftView=iv ;
    self.locationTextField.useDarkTheme = YES;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"InfoCircle"]];
    [imageView setFrame:CGRectMake(-7, -7, 40, 40) ];
    self.bioTextView.textContainerInset = UIEdgeInsetsMake(4, 19, 0, 0);
    self.bioTextView.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:68.0/255.0 blue:68.0/255.0 alpha:1.0];
    self.bioTextView.clipsToBounds = YES;
    self.bioTextView.layer.cornerRadius = 3.0f;
    [self.bioTextView addSubview:imageView];

}

-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addPictureTapped:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Photo Library",@"From Camera", nil];
    [sheet showInView:self.view];
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
        case 1:
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            break;
        case 2:
            return;
            break;
            
        default:
            break;
    }
    
    [self presentViewController:picker animated:YES completion:NULL];

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [[info valueForKey:UIImagePickerControllerOriginalImage]fixOrientation];
//    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.avatarButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.avatarButton setImage:image forState:UIControlStateNormal];
    self.userImage = image;
    [self dismissViewControllerAnimated:YES completion:NULL];
}



- (UIImage*) roundCorneredImage: (UIImage*) orig radius:(CGFloat) r {
    UIGraphicsBeginImageContextWithOptions(orig.size, NO, 0);
    [[UIBezierPath bezierPathWithRoundedRect:(CGRect){CGPointZero, orig.size}
                                cornerRadius:r] addClip];
    [orig drawInRect:(CGRect){CGPointZero, orig.size}];
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (IBAction)nextButtonTapped:(id)sender {
    [[CLYUserWebService sharedService] getCurrentUserWithCompletionHandler:^(CLYUser *user) {
        user.about = self.bioTextView.text;
        user.location = self.locationTextField.text;
        
        [[CLYUserWebService sharedService] putUser:user avatar:self.userImage background:nil completionHandler:^(CLYUser *user) {
            [[NSUserDefaults standardUserDefaults] setObject:user.avatar forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"friendsSegue" sender:self];
        }];
        
    }];
    
}

#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    //    NSDictionary* info = [aNotification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    CGRect frame = self.textView.frame;
    //    frame.size.height = frame.size.height - kbSize.height;
    //    self.textView.frame = frame;


    NSDictionary *info = [aNotification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];

    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];

    CGRect finalKeyboardFrame = [self.view convertRect:keyboardFrame fromView:self.view.window];

    int kbHeight = finalKeyboardFrame.size.height;

    int height = kbHeight;// + self.bottomConstraint.constant;

    self.bottomConstraint.constant = height;
    self.textViewConstraint.constant = 0;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    

    self.bottomConstraint.constant = 0;
    self.textViewConstraint.constant = 120;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	[self.bioTextView becomeFirstResponder];
	return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Bio"]) {
        textView.text = @"";
    }

}

@end
