//
//  CLYFindFriendCell.m
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYFindFriendCell.h"
#import "CLYApplicationConstants.h"
#import "CLYUserWebService.h"

@implementation CLYFindFriendCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
//    self.followButton.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.followButton.layer.borderWidth = 1.0;
    self.friendNameLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.friendLocationLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:10.5];

    
    self.contentView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.followButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    self.followButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:10.5];
    self.followButton.layer.cornerRadius = 3.0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)followButtonTapped:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"Follow"]) {
        [[CLYUserWebService sharedService] followUserWithId:self.userId];
        [button setTitle:@"Unfollow" forState:UIControlStateNormal];
    }else{
        [[CLYUserWebService sharedService] unfollowUserWithId:self.userId];
        [button setTitle:@"Follow" forState:UIControlStateNormal];
    }

    
}

-(void)prepareForReuse
{
    self.friendNameLabel.text = @"";
    self.friendLocationLabel.text = @"";
    self.friendImageView.image = nil;
    [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
}

@end
