//
//  CLYSignUpAccountViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSignUpAccountViewController.h"
#import "CLYApplicationConstants.h"
#import "CLYUserWebService.h"
#import "AFOAuth2Client.h"
#import "KeychainItemWrapper.h"
#import "CLYUser.h"

@interface CLYSignUpAccountViewController ()

- (IBAction)loginTapped:(id)sender;
- (IBAction)nextButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *signupLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;


@end

@implementation CLYSignUpAccountViewController

-(void)viewDidAppear:(BOOL)animated
{
    //TODO: XXX REMOVE THIS METHOD AFTER TESTING
//    [self performSegueWithIdentifier:@"communitySegue" sender:self];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self registerForKeyboardNotifications];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];

    self.view.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    self.signupLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:20.0];
    
    self.nameTextField.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.nameTextField.delegate = self;
    
    self.emailTextField.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.emailTextField.delegate = self;
    
    self.usernameTextField.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.usernameTextField.delegate = self;
    
    self.passwordTextField.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.passwordTextField.delegate = self;
    
    self.nextButton.layer.cornerRadius = 3.0;
    self.nextButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    
    self.loginButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.0];
    
    self.emailTextField.tintColor = [UIColor blackColor];
    self.usernameTextField.tintColor = [UIColor blackColor];
    self.passwordTextField.tintColor = [UIColor blackColor];
    self.nameTextField.tintColor = [UIColor blackColor];
    
    self.title = @"";
}
-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginTapped:(id)sender {
    
    UIStoryboard *signUp = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *vc = [signUp instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
//    //    NSDictionary* info = [aNotification userInfo];
//    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    //
//    //    CGRect frame = self.textView.frame;
//    //    frame.size.height = frame.size.height - kbSize.height;
//    //    self.textView.frame = frame;
//    
//    
//    NSDictionary *info = [aNotification userInfo];
//    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
//    
//    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    CGRect keyboardFrame = [kbFrame CGRectValue];
//    
//    CGRect finalKeyboardFrame = [self.view convertRect:keyboardFrame fromView:self.view.window];
//    
//    int kbHeight = finalKeyboardFrame.size.height;
//    
//    int height = kbHeight;// + self.bottomConstraint.constant;
//    
//    self.bottomConstraint.constant = height;
//    
//    [UIView animateWithDuration:animationDuration animations:^{
//        [self.view layoutIfNeeded];
//    }];
}

-(void) bumpViewByIndex:(int)fieldIndex
{
    
    NSTimeInterval animationDuration = 0.5;
    
    int height = 20 + fieldIndex * 64;
    
    self.bottomConstraint.constant = height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.bottomConstraint.constant = 20;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.nameTextField) {
        [self bumpViewByIndex:0];
	}
	if (textField == self.emailTextField) {
        [self bumpViewByIndex:1];
	}
	if (textField == self.usernameTextField) {
        [self bumpViewByIndex:2];
	}
    if (textField == self.passwordTextField) {
        [self bumpViewByIndex:3];
    }
}
 

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.nameTextField) {
		[self.emailTextField becomeFirstResponder];
        [self bumpViewByIndex:1];
	}
	if (textField == self.emailTextField) {
		[self.usernameTextField becomeFirstResponder];
        [self bumpViewByIndex:2];
	}
	if (textField == self.usernameTextField) {
		[self.passwordTextField becomeFirstResponder];
        [self bumpViewByIndex:3];
	}
    if (textField == self.passwordTextField) {
        [self nextButtonTapped:self];
    }
    
	[textField resignFirstResponder];
	return YES;
}

- (IBAction)nextButtonTapped:(id)sender
{
    if (self.nameTextField.text.length ==0) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter a name" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
        return;
    } else     if (self.emailTextField.text.length ==0) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter an email address" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
        return;
    } else    if (self.usernameTextField.text.length ==0) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter a username" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
        return;
    } else    if (self.passwordTextField.text.length ==0) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter a password" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
        return;
    }
    
    [[CLYUserWebService sharedService] createUserWithName:self.nameTextField.text emailAddress:self.emailTextField.text username:self.usernameTextField.text password:self.passwordTextField.text completionHandler:^(NSObject *object) {
        if ([object isKindOfClass:[CLYUser class]]) {
            //User successfully created
            [self performLogin];
        }else{
            //There was an error
            if ([object isKindOfClass:[NSDictionary class]]) {
                [self performSelectorOnMainThread:@selector(showErrorsFromDictionary:) withObject:object waitUntilDone:NO];
            }else{
                //Unknown Error
                [self performSelectorOnMainThread:@selector(showUnknownError) withObject:nil waitUntilDone:NO];
            }
        }
    }];
  
}

-(void) performLogin
{
    NSURL *url = [NSURL URLWithString:@"https://communly.herokuapp.com"];
    NSString *username = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    
    NSString *kClientID = [CLYApplicationConstants client_id];
    NSString *kClientSecret = [CLYApplicationConstants client_secret ];
    
    AFOAuth2Client *oauthClient = [AFOAuth2Client clientWithBaseURL:url clientID:kClientID secret:kClientSecret];
    [oauthClient authenticateUsingOAuthWithURLString:@"https://communly.herokuapp.com/oauth/token.json"
                                            username:username
                                            password:password
                                               scope:@"email"
                                             success:^(AFOAuthCredential *credential) {
//                                                 NSLog(@"I have a token! %@", credential.accessToken);
                                                 [AFOAuthCredential storeCredential:credential withIdentifier:oauthClient.serviceProviderIdentifier];
                                                 KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"CommunlyLogin" accessGroup:nil];
                                                 [keychainItem setObject:password forKey:(__bridge id)kSecValueData];
                                                 [keychainItem setObject:username forKey:(__bridge id)kSecAttrAccount];
                                                 
                                                 //TODO: Fetch User object/avatar now?
                                                 [[CLYUserWebService sharedService] getCurrentUserWithCompletionHandler:^(CLYUser *user){
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.avatar forKey:@"avatar"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"userfullname"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.username forKey:@"username"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.id forKey:@"userId"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                 }];
                                                 
                                                 [self performSelectorOnMainThread:@selector(showFeaturedCommunities) withObject:nil waitUntilDone:NO];
                                                 
                                             }
                                             failure:^(NSError *error) {
                                                 [self performSelectorOnMainThread:@selector(showLoginError) withObject:nil waitUntilDone:NO];
                                                 
                                                 NSLog(@"Error: %@", error);
                                             }];
}

-(void) showFeaturedCommunities
{
    [self performSegueWithIdentifier:@"communitySegue" sender:self];
}

-(void) showLoginError
{
     [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your account was successfully created but there was an error logging you in." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
}

-(void) showUnknownError
{
    [[[UIAlertView alloc]initWithTitle:@"Error" message:@"There was an unknown error creating your account." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
}

-(void) showErrorsFromDictionary:(NSDictionary *)errors
{
    NSString *fullErrorString = @"";
    
    if ([errors objectForKey:@"email"]) {
        for (NSString *error in [errors objectForKey:@"email"]) {
            NSString *errorString = [NSString stringWithFormat:@"Email %@.", error];
            fullErrorString = [fullErrorString stringByAppendingFormat:@"%@\n",errorString];
        }
    }

    if ([errors objectForKey:@"username"]) {
        for (NSString *error in [errors objectForKey:@"username"]) {
            NSString *errorString = [NSString stringWithFormat:@"Username %@.", error];
            fullErrorString = [fullErrorString stringByAppendingFormat:@"%@\n",errorString];
        }
    }

    if ([errors objectForKey:@"name"]) {
        for (NSString *error in [errors objectForKey:@"name"]) {
            NSString *errorString = [NSString stringWithFormat:@"Name  %@.", error];
            fullErrorString = [fullErrorString stringByAppendingFormat:@"%@\n",errorString];
        }
    }

    if ([errors objectForKey:@"password"]) {
        for (NSString *error in [errors objectForKey:@"password"]) {
            NSString *errorString = [NSString stringWithFormat:@"Password %@.", error];
            fullErrorString = [fullErrorString stringByAppendingFormat:@"%@\n",errorString];
        }
    }

    [[[UIAlertView alloc]initWithTitle:@"Error" message:fullErrorString delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];


}


@end
