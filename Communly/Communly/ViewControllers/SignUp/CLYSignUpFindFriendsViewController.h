//
//  CLYSignUpFindFriendsViewController.h
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"

@interface CLYSignUpFindFriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
