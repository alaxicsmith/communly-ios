//
//  CLYLoginViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYLoginViewController.h"
#import "AFOAuth2Client.h"
#import "KeychainItemWrapper.h"
#import "CLYUserWebService.h"
#import "CLYUser.h"
#import "CLYApplicationConstants.h"
#import "CLYSessionManager.h"

@interface CLYLoginViewController ()
- (IBAction)forgotPasswordTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end

@implementation CLYLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self registerForKeyboardNotifications];
    
    self.title = @"";

    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self.navigationItem setLeftBarButtonItem:backButton animated:YES];

    self.loginLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:20.0];
    
    self.loginButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.loginButton.layer.cornerRadius = 3.0;
    
    self.forgotPasswordButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.0];
    
    self.emailAddress.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.emailAddress.delegate = self;

    self.password.font = [UIFont fontWithName:@"ProximaNova-Light" size:17.5];
    self.password.delegate = self;
    
    self.emailAddress.tintColor = [UIColor blackColor];
    self.password.tintColor = [UIColor blackColor];
    self.emailAddress.textColor = [UIColor blackColor];
    self.password.textColor = [UIColor blackColor];

  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"https://communly.herokuapp.com"];
    NSString *username = self.emailAddress.text;
    NSString *password = self.password.text;
    
    NSString *kClientID = [CLYApplicationConstants client_id];
    NSString *kClientSecret = [CLYApplicationConstants client_secret ];
    
    AFOAuth2Client *oauthClient = [AFOAuth2Client clientWithBaseURL:url clientID:kClientID secret:kClientSecret];
    [oauthClient authenticateUsingOAuthWithURLString:@"https://communly.herokuapp.com/oauth/token.json"
                                            username:username
                                            password:password
                                               scope:@"email"
                                             success:^(AFOAuthCredential *credential) {
//                                                 NSLog(@"I have a token! %@", credential.accessToken);
                                                 [AFOAuthCredential storeCredential:credential withIdentifier:oauthClient.serviceProviderIdentifier];
                                                 
                                                 KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"CommunlyLogin" accessGroup:nil];
                                                 [keychainItem setObject:password forKey:(__bridge id)kSecValueData];
                                                 [keychainItem setObject:username forKey:(__bridge id)kSecAttrAccount];
                                                 
                                                 [[CLYSessionManager sharedManager] resetToken];
                                                 
                                                 [[CLYUserWebService sharedService] getCurrentUserWithCompletionHandler:^(CLYUser *user){
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.avatar forKey:@"avatar"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"userfullname"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.username forKey:@"username"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:user.id forKey:@"userId"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:self.emailAddress.text forKey:@"email"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];

                                                     [self performSelectorOnMainThread:@selector(presentUserStreamView) withObject:nil waitUntilDone:NO];
                                                 }];
                                                 

                                                 
                                             }
                                             failure:^(NSError *error) {
                                                [self performSelectorOnMainThread:@selector(showInvalidPasswordAlert) withObject:nil waitUntilDone:NO];
                                                 NSLog(@"Error: %@", error);
                                             }];

    
    
    
    
}

-(void)showInvalidPasswordAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Login" message:@"Either the specified Email Address or Password is invalid.  Please try again." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

-(void) presentUserStreamView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_USER" object:nil];
    UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Stream" bundle:nil];
    UIViewController *vc = [stream instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];

}

-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)forgotPasswordTapped:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://communly.com/users/password/new"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
    
}


#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    //    NSDictionary* info = [aNotification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    CGRect frame = self.textView.frame;
    //    frame.size.height = frame.size.height - kbSize.height;
    //    self.textView.frame = frame;
    
    
    NSDictionary *info = [aNotification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGRect finalKeyboardFrame = [self.view convertRect:keyboardFrame fromView:self.view.window];
    
    int kbHeight = finalKeyboardFrame.size.height;
    
    int height = kbHeight;// + self.bottomConstraint.constant;
    
    self.bottomConstraint.constant = height + 10.0;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.bottomConstraint.constant = 175;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.emailAddress) {
		[self.password becomeFirstResponder];
	}
    
    if (textField == self.password) {
        [self login:textField];
    }
    
	[textField resignFirstResponder];
	return YES;
}


@end
