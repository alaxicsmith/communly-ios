//
//  CLYLaunchViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYLaunchViewController.h"
#import "CLYApplicationConstants.h"
#import "UIViewController+MMDrawerController.h"
#import "AFOAuth2Client.h"
#import <AVFoundation/AVFoundation.h>

#import "CLYSignUpFindFriendsViewController.h"
#import "CLYSignUpCommunitiesViewController.h"
#import "CLYSignUpPersonalInfoViewController.h"

@interface CLYLaunchViewController ()
- (IBAction)createNewAccountTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *communlyHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *communlyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *termsLabel;

@property (strong, nonatomic)  AVPlayer *avPlayer;

@end

@implementation CLYLaunchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.communlyHeaderLabel.font = [UIFont fontWithName:@"ProximaNova-Bold" size:39];
    self.communlyDescriptionLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:14.5];

    self.createAccountButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    self.createAccountButton.layer.cornerRadius = 3.0;
    self.createAccountButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:15];
    
    self.loginButton.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    self.loginButton.layer.cornerRadius = 3.0;
    self.loginButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:15];
    
  
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"By logging in, you agree to \nthe Communly Terms of Service and Privacy Policy"];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Light" size:12.5] range:NSMakeRange(0, 42)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Bold" size:12.5] range:NSMakeRange(42, 16)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Light" size:12.5] range:NSMakeRange(58, 5)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Bold" size:12.5] range:NSMakeRange(63, 14)];

    [string addAttribute:NSForegroundColorAttributeName value: [UIColor lightGrayColor] range:NSMakeRange(0, string.length)];
    
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init ];
    pStyle.alignment = NSTextAlignmentCenter;
    pStyle.lineSpacing = 5.0;
    [string addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, string.length)];
    
    [self.termsLabel setAttributedText:string];
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.barTintColor = [CLYApplicationConstants applicationBackgroundColor];
    
    self.navigationController.navigationBar.translucent=NO;

    //TODO: XXX REENABLE!
    // If we have good credentials, go to the stream view directly
    AFOAuthCredential *oauthCredential = [AFOAuthCredential retrieveCredentialWithIdentifier:@"communly.herokuapp.com"];
    if(oauthCredential && [oauthCredential.accessToken length] > 0 ){
        UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Stream" bundle:nil];
        UIViewController *vc = [stream instantiateInitialViewController];
        [self.mm_drawerController setCenterViewController:vc];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(replayVideo) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"video" ofType:@"mov"];
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.avPlayer = [AVPlayer playerWithURL:fileURL];
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    layer.frame = self.view.frame;
    [self.backgroundImageView.layer addSublayer: layer];
    
    [self.avPlayer play];
    
    
}

-(IBAction)termsOfServiceTapped:(id)sender
{
    //
//    UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Signup" bundle:nil];
////    CLYSignUpFindFriendsViewController *vc = [stream instantiateViewControllerWithIdentifier:@"CLYSignUpFindFriendsViewController"];
//    CLYSignUpPersonalInfoViewController *vc = [stream instantiateViewControllerWithIdentifier:@"CLYSignUpPersonalInfoViewController"];
//    //CLYSignUpCommunitiesViewController *vc =[stream instantiateViewControllerWithIdentifier:@"CLYSignUpCommunitiesViewController"];
//    [self presentViewController:vc animated:YES completion:nil];

    NSURL *url = [NSURL URLWithString:@"http://communly.com/terms"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }

}

-(IBAction)privacyPolicyTapped:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://communly.com/privacy"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
    
}

-(void)replayVideo
{
    [self.avPlayer seekToTime:kCMTimeZero];
//    [self.avPlayer play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)createNewAccountTapped:(id)sender {
    UIStoryboard *signUp = [UIStoryboard storyboardWithName:@"Signup" bundle:nil];
    UIViewController *vc = [signUp instantiateInitialViewController];
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
