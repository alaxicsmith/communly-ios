//
//  CLYSettingsTableViewCell.h
//  Communly
//
//  Created by Kelli Ireland on 2014/03/25.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYSettingsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *settingTitle;

@property (weak, nonatomic) IBOutlet UILabel *settingValue;

@end
