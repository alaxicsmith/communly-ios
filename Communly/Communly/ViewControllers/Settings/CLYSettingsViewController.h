//
//  CLYSettingsViewController.h
//  Communly
//
//  Created by Kelli Ireland on 2014/03/25.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYViewController.h"
#import "CLYUser.h"

@interface CLYSettingsViewController : CLYViewController

@end
