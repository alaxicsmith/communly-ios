//
//  CLYSettingsPictureCell.m
//  Communly
//
//  Created by Chris Allwein on 6/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSettingsPictureCell.h"

@implementation CLYSettingsPictureCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
        self.settingsLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
