//
//  CLYSettingsAboutCell.m
//  Communly
//
//  Created by Chris Allwein on 7/2/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSettingsAboutCell.h"

@implementation CLYSettingsAboutCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
        self.textView.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self setSelected:YES];
}

@end
