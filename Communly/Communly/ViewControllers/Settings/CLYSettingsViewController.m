//
//  CLYSettingsViewController.m
//  Communly
//
//  Created by Kelli Ireland on 2014/03/25.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSettingsViewController.h"
#import "CLYSettingsExternalConnectionTableViewCell.h"
#import "CLYSettingsTableViewCell.h"
#import "CLYUser.h"
#import "CLYUserWebService.h"
#import "CLYSettingsPictureCell.h"
#import "UIImageView+AFNetworking.h"
#import "CLYSettingsAboutCell.h"
#import "CLYApplicationConstants.h"
#import "UIImage+fixOrientation.h"

@interface CLYSettingsViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) CLYUser *user;

@property (weak,nonatomic) UITextView *aboutTextView;
@property (nonatomic) int picMode; //0 is avatar, 1 is background
@property (nonatomic,strong) UIView *aboutAccessoryView;
@end



@implementation CLYSettingsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    self.title = @"Settings";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    self.view.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
        self.tableView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    _aboutAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
    _aboutAccessoryView.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    UIButton *_commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _commentButton.frame = CGRectMake(227, 8, 73, 28);
    NSAttributedString *attrButtonTitle = [[NSAttributedString alloc] initWithString:@"Save" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-SemiBold" size:12.0], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [_commentButton setAttributedTitle:attrButtonTitle forState:UIControlStateNormal];
    _commentButton.layer.cornerRadius = 3.0;
    _commentButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    [_commentButton addTarget:self action:@selector(saveAbout) forControlEvents:UIControlEventTouchUpInside];
    
    [_aboutAccessoryView addSubview:_commentButton];
    
    
    [[CLYUserWebService sharedService] getUserWithId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] completionHandler:^(CLYUser *user) {
        self.user = user;
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==5) {
        return 125.0;
    }
    return 54.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    bool facebookConnected = YES; // TODO: Get from user settings
    if (section == 0) return 6;                             // TODO: Get from user settings
    if (section == 1) return (facebookConnected ? 1 : 0);   // Facebook connected
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"Cell for row at index path: %@", indexPath.description);
    
    
    NSArray *settingsList = @[@"Full Name",
                              @"Email",
                              @"Location",
                              @"Profile Picture",
                              @"Background",
                              @"Bio"
                              ];
    
    
    if(indexPath.section ==0) {
        if (indexPath.row == 3 || indexPath.row ==4) {
            CLYSettingsPictureCell *cell = (CLYSettingsPictureCell *)[tableView dequeueReusableCellWithIdentifier:@"CLYSettingsPictureCell"];
            if(cell == nil){
                cell = [[CLYSettingsPictureCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CLYSettingsPictureCell"];
            }
            
            cell.settingsLabel.text = settingsList[indexPath.row];
            
            switch (indexPath.row) {
                case 3:
                    [cell.settingsImage setImageWithURL:[NSURL URLWithString:self.user.avatar]];
                    break;
                case 4:
                    [cell.settingsImage setImageWithURL:[NSURL URLWithString:self.user.background]];
                    break;

                default:
                    break;
            }
            
            return cell;

        }
        
        if (indexPath.row == 5) {
             CLYSettingsAboutCell *cell = (CLYSettingsAboutCell *)[tableView dequeueReusableCellWithIdentifier:@"CLYSettingsAboutCell"];
            cell.textView.text = self.user.about;
            self.aboutTextView = cell.textView;
            self.aboutTextView.delegate = self;
            [self.aboutTextView setInputAccessoryView:_aboutAccessoryView];

            return cell;
        }
        
        CLYSettingsTableViewCell *cell = (CLYSettingsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
        if(cell == nil){
            cell = [[CLYSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"settingsCell"];
        }
        
        cell.settingTitle.text = settingsList[indexPath.row];
        
        switch (indexPath.row) {
            case 0:
                cell.settingValue.text = self.user.name;
                break;
            case 1:
                cell.settingValue.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
                break;
            case 2:
                cell.settingValue.text = self.user.location;
                break;
    
            default:
                break;
        }
        
        return cell;
    }else{
        CLYSettingsExternalConnectionTableViewCell* cell = (CLYSettingsExternalConnectionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"settingsExternalConnectionCell"];
        if(cell == nil){
            cell = [[CLYSettingsExternalConnectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"settingsExternalConnectionCell"];
        }
        
        return cell;
    }
    
    
}

// Modified from http://stackoverflow.com/questions/18822619/ios-7-tableview-like-in-settings-app-on-ipad
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        if (tableView == self.tableView) {
            CGFloat cornerRadius = 3.0f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.f].CGColor;
            
            // Custom background
            if (indexPath.section == 1){
                
                layer.fillColor = [UIColor colorWithRed:59.0/255.0 green:89.0/255.0 blue:152.0/255.0 alpha:1.0].CGColor;
            }
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            
            testView.backgroundColor = UIColor.clearColor;
            
            cell.backgroundView = testView;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Endpoint" message:@"This functionality has not been implemented because the endpoint is missing or broken." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    switch (indexPath.row) {
        case 0:
            [self handleName];
            break;
        case 1:
//            [self handleEmail];
            break;
        case 2:
            [self handleLocation];
            break;
        case 3:
            [self handleAvatar];
            break;
        case 4:
            [self handleBackground];
            break;
        case 5:
            [self handleBio];
            break;
            
        default:
            break;
    }
    
}

-(void)handleName
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Full Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].text = self.user.name;
    [alert textFieldAtIndex:0].tintColor = [UIColor blackColor];
    [alert show];
}

-(void)handleEmail
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].text = self.user.email;
    [alert textFieldAtIndex:0].tintColor = [UIColor blackColor];
    [alert show];
}

-(void)handleLocation
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].text = self.user.location;
    [alert textFieldAtIndex:0].tintColor = [UIColor blackColor];
    [alert show];
}

-(void)handleAvatar
{
    self.picMode = 0;
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Photo Library",@"From Camera", nil];
    [sheet showInView:self.view];
}

-(void)handleBackground
{
    self.picMode = 1;
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Photo Library",@"From Camera", nil];
    [sheet showInView:self.view];
}

-(void)handleBio
{
    [self.aboutTextView becomeFirstResponder];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    NSLog(@"Alert Button: %li",(long)buttonIndex);
    //0 is cancel.  1 is save
    if (buttonIndex == 0) {
        return;
    }
    
    if ([alertView.title  isEqual: @"Full Name"]) {
        self.user.name = [alertView textFieldAtIndex:0].text;
    }
    
    if ([alertView.title  isEqual: @"Email"]) {
        self.user.email = [alertView textFieldAtIndex:0].text;
    }
    
    if ([alertView.title  isEqual: @"Location"]) {
        self.user.location = [alertView textFieldAtIndex:0].text;
    }

    [[CLYUserWebService sharedService] putUser:self.user avatar:nil background:nil completionHandler:^(CLYUser *user) {}];
    [self.tableView reloadData];
    return;

}

-(void)saveAbout
{
    self.user.about = self.aboutTextView.text;
    [[CLYUserWebService sharedService] putUser:self.user avatar:nil background:nil completionHandler:^(CLYUser *user) {}];
    [self.tableView reloadData];
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    [self.aboutTextView resignFirstResponder];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        break;
        case 1:
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        break;
        case 2:
        return;
        break;
        
        default:
        break;
    }
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [[info valueForKey:UIImagePickerControllerOriginalImage]fixOrientation];
//    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    if (self.picMode == 0) {
        CLYSettingsPictureCell *cell = (CLYSettingsPictureCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        cell.settingsImage.contentMode = UIViewContentModeScaleAspectFit;
        cell.settingsImage.image = image;
        [[CLYUserWebService sharedService] putUser:self.user avatar:image background:nil completionHandler:^(CLYUser *user) {
            [[CLYUserWebService sharedService] getCurrentUserWithCompletionHandler:^(CLYUser *user){
                [[NSUserDefaults standardUserDefaults] setObject:user.avatar forKey:@"avatar"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }];
        }];

    }else{
        CLYSettingsPictureCell *cell = (CLYSettingsPictureCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        cell.settingsImage.contentMode = UIViewContentModeScaleAspectFit;
        cell.settingsImage.image = image;
        [[CLYUserWebService sharedService] putUser:self.user avatar:nil background:image completionHandler:^(CLYUser *user) {}];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 300, 0);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
   }

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}
@end
