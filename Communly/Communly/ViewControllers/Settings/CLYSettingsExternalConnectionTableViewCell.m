//
//  CLYSettingsExternalConnectionTableViewCell.m
//  Communly
//
//  Created by Kelli Ireland on 2014/03/25.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSettingsExternalConnectionTableViewCell.h"

@interface CLYSettingsExternalConnectionTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *externalConnectionName;


@end
@implementation CLYSettingsExternalConnectionTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    //self.contentView.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:89.0/255.0 blue:152.0/255.0 alpha:1.0];
    self.externalConnectionName.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
