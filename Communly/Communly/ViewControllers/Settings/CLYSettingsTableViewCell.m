//
//  CLYSettingsTableViewCell.m
//  Communly
//
//  Created by Kelli Ireland on 2014/03/25.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSettingsTableViewCell.h"

@implementation CLYSettingsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    //self.contentView.backgroundColor = [UIColor whiteColor];
    self.settingTitle.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12];
    self.settingValue.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
