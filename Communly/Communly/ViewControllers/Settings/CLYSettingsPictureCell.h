//
//  CLYSettingsPictureCell.h
//  Communly
//
//  Created by Chris Allwein on 6/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYSettingsPictureCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *settingsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsImage;

@end
