//
//  CLYNotificationCell.m
//  Communly
//
//  Created by Chris Allwein on 5/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYNotificationCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYNotificationCell

- (void)awakeFromNib
{
    self.contentView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.actionLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:12.5];
    self.actionLabel.textColor = [UIColor whiteColor];

    self.userImageView.layer.cornerRadius = 3.0;
    self.userImageView.clipsToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
