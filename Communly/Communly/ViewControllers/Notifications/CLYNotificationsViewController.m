//
//  CLYNotificationsViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYNotificationsViewController.h"
#import "CLYUserWebService.h"
#import "CLYNotificationCell.h"
#import "CLYNotification.h"
#import "UIImageView+AFNetworking.h"
#import "CLYHelperFunctions.h"
#import "CLYApplicationConstants.h"
#import "CLYPostViewController.h"
#import "CLYUserViewController.h"
#import "CLYCommunityViewController.h"

@interface CLYNotificationsViewController ()

@property (nonatomic, strong) NSArray *notifications;

@end

@implementation CLYNotificationsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Notifications";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    self.notifications = [NSArray array];
    
    self.tableView.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    
    [[CLYUserWebService sharedService] getNotificationsForCurrentUserWithCompletionHandler:^(NSArray *notifications) {
        self.notifications = notifications;
        [self.tableView reloadData];
    }];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notifications count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellID = @"CLYNotificationCell";
    
    CLYNotificationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellID];
       
    CLYNotification *n = [self.notifications objectAtIndex:indexPath.row];
    [cell.userImageView setImageWithURL:[NSURL URLWithString:n.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  %@", n.user.name, [CLYHelperFunctions timePostedFormatted:n.notificationDate]]];
    [attrString addAttributes:@{ NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:14.0] } range:NSMakeRange(0, n.user.name.length+1)];
    
    [attrString addAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:10.0] } range:NSMakeRange(n.user.name.length, attrString.length-n.user.name.length)];
    
    cell.userLabel.attributedText = attrString;
    
    cell.actionLabel.attributedText = n.notificationContent;
    cell.actionImageView.image = n.notificationTypeImage;

    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYNotification *n = [self.notifications objectAtIndex:indexPath.row];
    
    if ([n.notificationAction isEqualToString:@"Favorite"] || [n.notificationAction isEqualToString:@"Comment"]) {
        UIStoryboard *post = [UIStoryboard storyboardWithName:@"Post" bundle:nil];
        CLYPostViewController *vc = [post instantiateViewControllerWithIdentifier:@"CLYPostViewController"];
        vc.postId = n.postId;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    if ([n.notificationAction isEqualToString:@"User"]  ) {
        UIStoryboard *post = [UIStoryboard storyboardWithName:@"User" bundle:nil];
        CLYUserViewController *vc = [post instantiateViewControllerWithIdentifier:@"CLYUserViewController"];
        vc.userId = n.userId;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    if ([n.notificationAction isEqualToString:@"Community"] ) {
        UIStoryboard *post = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
        CLYCommunityViewController *vc = [post instantiateViewControllerWithIdentifier:@"CLYCommunityViewController"];
        vc.communityId = n.communityId;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}



@end
