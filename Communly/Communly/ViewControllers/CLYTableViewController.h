//
//  CLYTableViewController.h
//  Communly
//
//  Created by Chris Allwein on 4/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"

@interface CLYTableViewController : UITableViewController

@end
