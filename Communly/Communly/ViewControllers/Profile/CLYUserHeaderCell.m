//
//  CLYUserHeaderCell.m
//  Communly
//
//  Created by Chris Allwein on 6/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUserHeaderCell.h"
#import "CLYApplicationConstants.h"
#import "CLYUserWebService.h"
#import <QuartzCore/QuartzCore.h>

@implementation CLYUserHeaderCell


- (void)awakeFromNib
{
    // Initialization code
    self.memberCountLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    self.typeLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:11.0];
    
    self.communityNameLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];
    self.communityTypeLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.bannerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    
    self.communityAvatarImageView.layer.cornerRadius = 3.0;
    self.communityAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.communityAvatarImageView.layer.borderWidth = 1.0;
    self.communityAvatarImageView.clipsToBounds = YES;
}

 
@end
