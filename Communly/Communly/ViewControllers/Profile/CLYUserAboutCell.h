//
//  CLYUserAboutCell.h
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYLocationLabel.h"

@interface CLYUserAboutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet CLYLocationLabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageView;

@end
