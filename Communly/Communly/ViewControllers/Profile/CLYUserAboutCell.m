//
//  CLYUserAboutCell.m
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUserAboutCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYUserAboutCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.aboutLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.locationLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
  
    self.aboutLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.locationLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    
//    self.locationImageView.image = [UIImage imageNamed:@"LocationMarker"];
//    self.locationImageView.contentMode = UIViewContentModeCenter;
    


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
