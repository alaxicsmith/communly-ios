//
//  CLYUserHeaderCell.h
//  Communly
//
//  Created by Chris Allwein on 6/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYUserHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *communityBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *communityAvatarImageView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UILabel *memberCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *membersImageView;
@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;
@property (weak, nonatomic) IBOutlet UILabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *communityTypeLabel;
@property (strong, nonatomic) NSString *userId;


@end
