//
//  CLYUserViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUserViewController.h"
#import "CLYUserHeaderCell.h"
#import "CLYCommunity.h"
#import "CLYUserWebService.h"
#import "CLYPostsWebService.h"
#import "UIImageView+AFNetworking.h"
#import "CLYStreamCell.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYPostViewController.h"
#import "CLYCommunityViewController.h"
#import "CLYUserAboutCell.h"
#import "CLYUserCountsCell.h"
#import "CLYHelperFunctions.h"

@interface CLYUserViewController ()

@property (nonatomic,strong) CLYUser *user;
@property (nonatomic,strong) NSArray *posts;

@end

@implementation CLYUserViewController

-(void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Profile";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    
    self.tableView.hidden = YES;
      
    [[CLYUserWebService sharedService] getUserWithId:self.userId completionHandler:^(CLYUser *user) {
        self.user = user;
        [self.tableView reloadData];
        self.tableView.hidden = NO;
    }];
    
    [[CLYPostsWebService sharedService] getPostsWithUserId:self.userId  completionHandler:^(NSArray *posts) {
        self.posts = posts;
        [self.tableView reloadData];
    }];
    

}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.image = [UIImage imageNamed:@"hamburger"];
    leftDrawerButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    UIBarButtonItem *composeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"compose"] style:UIBarButtonItemStylePlain target:self action:@selector(compose)];
    composeButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 15);
    [self.navigationItem setRightBarButtonItem:composeButton animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCommunity:) name:@"OPEN_COMMUNITY" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openUser:) name:@"OPEN_USER" object:nil];

    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)openCommunity:(NSNotification*)sender
{
//    [_commentTextField resignFirstResponder];
//    [_hiddenCommentTextField resignFirstResponder];
  
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
    CLYCommunityViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYCommunityViewController"];
    vc.communityId = sender.object;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)openUser:(NSNotification*)sender
{
//    NSLog(@"UserViewController openUser: %@",sender.object);
    
//    [_commentTextField resignFirstResponder];
//    [_hiddenCommentTextField resignFirstResponder];
    
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"User" bundle:nil];
    CLYUserViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYUserViewController"];
    vc.userId = sender.object;

    [self.navigationController setViewControllers:@[vc] animated:YES];
//    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)compose
{
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"CreatePost" bundle:nil];
    UIViewController *vc = [post instantiateInitialViewController];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    }else
    {
        return [self.posts count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CLYUserHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLYUserHeaderCell" forIndexPath:indexPath];
            
            cell.userId = self.user.id;
            cell.communityNameLabel.text = self.user.name;
            cell.communityTypeLabel.text = self.user.username;
            [cell.communityBackgroundImageView setImageWithURL:[NSURL URLWithString:self.user.background]];
            [cell.communityAvatarImageView setImageWithURL:[NSURL URLWithString:self.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
            return cell;
        }else if (indexPath.row == 1) {
            CLYUserAboutCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLYUserAboutCell" forIndexPath:indexPath];
            cell.aboutLabel.text = self.user.about;
            
            if (self.user.location) {
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"userLocation"];
                attachment.bounds = CGRectMake(0, -6, 21, 21);
                NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];

                NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:self.user.location];
                [attachmentString appendAttributedString:myString];
                
                cell.locationLabel.attributedText = attachmentString;
            }
 

            return cell;
        }else {
            CLYUserCountsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLYUserCountsCell" forIndexPath:indexPath];
            cell.postsCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self.posts count]];
            cell.followingCountLabel.text = [self.user.userFollowingCount stringValue];
            cell.followersCountLabel.text = [self.user.userFollowersCount stringValue];
            cell.communityCountLabel.text = [self.user.userCommunitiesCount stringValue];
            return cell;
        }

    }else
    {
        CLYPost *object = [self.posts objectAtIndex:indexPath.row];
        
        CLYStreamCell *streamCell = (CLYStreamCell*)[tableView dequeueReusableCellWithIdentifier:@"CLYStreamCell" forIndexPath:indexPath];
        streamCell.footerView.communityButton.hidden = NO;
        [streamCell.headerView.userImage setImageWithURL:[NSURL URLWithString:object.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
        streamCell.headerView.userNameLabel.text = object.user.name;
        streamCell.headerView.userId = object.user.id;
        streamCell.headerView.timeLabel.text = [CLYHelperFunctions timePostedFormatted:object.created_at];
        
        streamCell.postImageView.image = nil;
        [streamCell.postImageView setImageWithURL:[NSURL URLWithString:object.photo]];
        streamCell.footerView.postTextLabel.text = object.content;
        streamCell.footerView.communityId = object.community.id;
        streamCell.footerView.postId = object.id;
        streamCell.headerView.userId = object.user.id;

        if (object.photo && object.photo.length > 0 && ![object.photo isEqualToString:@"/photos/original/missing.png"]) {
            [streamCell.postImageView setImageWithURL:[NSURL URLWithString:object.photo]];
            streamCell.postImageView.contentMode = UIViewContentModeScaleAspectFill;
            streamCell.heightConstraint.constant = 275.0;
            streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
            streamCell.headerView.userNameLabel.textColor = [UIColor whiteColor];
            streamCell.headerView.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0];
            streamCell.headerView.alpha = 0.7;
            streamCell.headerView.timeLabel.textColor = [UIColor whiteColor];
            [self.view layoutIfNeeded];
        }else{
            streamCell.postImageView.image = nil;
            streamCell.heightConstraint.constant = 62.0;
            streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:15.0];
            streamCell.headerView.userNameLabel.textColor = [UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0];
            
            streamCell.headerView.timeLabel.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0];
            streamCell.headerView.backgroundColor = [UIColor clearColor];
            streamCell.headerView.alpha = 1.0;
            [self.view layoutIfNeeded];
        }

        
        streamCell.footerView.favesButton.favesCount= [object.favesCount intValue];
        
        NSString *communityString = [NSString stringWithFormat:@"community: %@", object.community.name];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:communityString];
        
        [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Regular" size:12] range:NSMakeRange(0, attString.length)];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0] range:NSMakeRange(10, attString.length-10)];
        NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init ];
        pStyle.alignment = NSTextAlignmentLeft;
        [attString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, attString.length)];
        streamCell.footerView.communityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [streamCell.footerView.communityButton setAttributedTitle:attString forState:UIControlStateNormal];
        
        return streamCell;
    
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
       
        if (indexPath.row == 0) {
             return 188;
        }else if (indexPath.row == 1) {
           return 72;
        }else {
            return 72;
        }
        
        
    }
    
    CLYPost *post = [self.posts objectAtIndex:indexPath.row];
    
    CGFloat header = 62.0;
    CGFloat image;
    
    if (post.photo && post.photo.length > 0 && ![post.photo isEqualToString:@"/photos/original/missing.png"]) {
        image = 275.0;
    }else{
        image = 0.0;
    }
    
    if(image>62.0)
    {
        image = image-header;
    }
    
    CGFloat content = [self heightWithPost:post];
    CGFloat footer = 44.0;
    
    CGFloat height = header + image + content + footer;
    //    NSLog(@"Height for row %i is %f", indexPath.row, height);
    
    
    return height;
    
}

-(CGFloat)heightForImage:(UIImage*)image inImageViewAspectFit:(UIImageView*)imageView
{
    if (!image) {
        return 0;
    }
    
    float imageRatio = image.size.width / image.size.height;
    
    float viewRatio = imageView.frame.size.width / imageView.frame.size.height;
    
    if(imageRatio < viewRatio)
    {
        return  imageView.frame.size.height;
    }
    else
    {
        float scale = imageView.frame.size.width / image.size.width;
        
        float height = scale * image.size.height;
        
        return  height;
    }
}

- (CGFloat) heightWithPost:(CLYPost *)post
{
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13.5f]];
    [calculationView setText:post.content];
    CGSize expectedSize = [calculationView sizeThatFits:CGSizeMake(290.0f, MAXFLOAT)];
    
    return ceil(expectedSize.height) + 7.0f * 2; // 7 px padding top & bottom
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return;
    }
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *postStoryBoard = [UIStoryboard storyboardWithName:@"Post" bundle:nil];
    CLYPostViewController *vc = [postStoryBoard instantiateInitialViewController];
    CLYPost *post = (CLYPost *)[self.posts objectAtIndex:indexPath.row];
    vc.postId = post.id;
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
