//
//  CLYUserCountsCell.m
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUserCountsCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYUserCountsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.postsCountLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];
    self.communityCountLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];
    self.followersCountLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];
    self.followingCountLabel.font = [UIFont fontWithName:@"ProximaNova-SemiBold" size:15.0];

    self.postsCountLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.communityCountLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.followersCountLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.followingCountLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    
    self.postsLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:11.5];
    self.communityLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:11.5];
    self.followersLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:11.5];
    self.followingLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:11.5];

    self.postsLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.communityLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.followersLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];
    self.followingLabel.textColor = [CLYApplicationConstants applicationBackgroundColor];


    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
