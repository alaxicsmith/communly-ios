//
//  CLYStreamCellHeaderView.h
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYStreamCellHeaderView : UIView

@property(nonatomic, strong) IBOutlet UIImageView *userImage;
@property(nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property(nonatomic, strong) IBOutlet UIImageView *timeImage;
@property(nonatomic, strong) IBOutlet UILabel *timeLabel;

@property(nonatomic, strong)  NSString *userId;
-(IBAction)openUser:(id)sender;

@end
