//
//  CLYStreamCellFooterView.h
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYFavesButton.h"

@interface CLYStreamCellFooterView : UIView

@property(nonatomic, strong) IBOutlet UILabel *postTextLabel;
@property(nonatomic, strong) IBOutlet UIButton *commentButton;
@property(nonatomic, strong) IBOutlet CLYFavesButton *favesButton;
@property(nonatomic, strong) IBOutlet UIButton *communityButton;

-(IBAction)openCommunity:(id)sender;
-(IBAction)openComment:(id)sender;
-(IBAction)favePost:(id)sender;
@property(nonatomic, strong)  NSString *communityId;
@property(nonatomic, strong)  NSString *postId;

@end
