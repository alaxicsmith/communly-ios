//
//  CLYStreamCell.h
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYStreamCellHeaderView.h"
#import "CLYStreamCellFooterView.h"

@interface CLYStreamCell : UITableViewCell

@property(nonatomic, strong) IBOutlet CLYStreamCellHeaderView *headerView;
@property(nonatomic, strong) IBOutlet UIImageView *postImageView;
@property(nonatomic, strong) IBOutlet CLYStreamCellFooterView *footerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@end
