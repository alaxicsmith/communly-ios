//
//  CLYStreamViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYStreamViewController.h"
#import "CLYStreamCell.h"
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"
#import "FetchedResultsControllerDataSource.h"
#import "CLYAsyncDataStore.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYCommunity.h"
#import "UIImageView+AFNetworking.h"
#import "CLYPostViewController.h"
#import "CLYCommunityViewController.h"
#import "CLYUserViewController.h"
#import "CLYCommentTextField.h"
#import "CLYApplicationConstants.h"
#import "CLYPostsWebService.h"
#import "CLYHelperFunctions.h"

@interface CLYStreamViewController ()<FetchedResultsControllerDataSourceDelegate>

@property (nonatomic, strong) FetchedResultsControllerDataSource *dataSource;
@property (nonatomic,strong) UITextField *hiddenCommentTextField;
@property (nonatomic,strong) UIView *commentAccessoryView;
@property (nonatomic,strong) CLYCommentTextField *commentTextField;
@property (nonatomic,strong) UIButton *commentButton;
@property (nonatomic,strong) NSString *commentPost;
@end

@implementation CLYStreamViewController

#pragma mark Initialization

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.image = [UIImage imageNamed:@"hamburger"];
    leftDrawerButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
 
    UIBarButtonItem *composeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"compose"] style:UIBarButtonItemStylePlain target:self action:@selector(compose)];
    composeButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 15);
    [self.navigationItem setRightBarButtonItem:composeButton animated:YES];
    
    
    self.managedObjectContext = [[CLYAsyncDataStore sharedStore] getManagedObjectContext];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CLYPost"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO]];
    self.dataSource = [[FetchedResultsControllerDataSource alloc] initWithTableView:self.tableView];
    self.dataSource.delegate = self;
    self.dataSource.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    self.dataSource.reuseIdentifier = @"streamCell";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCommunity:) name:@"OPEN_COMMUNITY" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openUser:) name:@"OPEN_USER" object:nil];

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) loadNewPosts
{
//    NSLog(@"Loading New Posts");
    
    [[CLYAsyncDataStore sharedStore] loadNewPosts];
    
//    NSLog(@"Performing Fetch");
    NSError *error;
    [self.dataSource.fetchedResultsController performFetch:&error];
    [self stopRefresh];
//    NSLog(@"Finish Refreshing");
    
     self.tableView.allowsSelection = YES;
    self.tableView.delegate = self;

}

- (void)stopRefresh
{
    [self.refreshControl endRefreshing];
}

- (void)deleteObject:(id)object
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIStoryboard *menuBoard = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
    UIViewController *menu = [menuBoard instantiateViewControllerWithIdentifier:@"menu"];
    self.mm_drawerController.leftDrawerViewController = menu;
    
    self.navigationItem.title = @"Stream";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5]}];
    

    
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0] }];
    refresh.tintColor = [UIColor whiteColor];
    [refresh addTarget:self action:@selector(loadNewPosts) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    [self loadNewPosts];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createComment:) name:@"CREATE_COMMENT" object:nil];
    [self registerForKeyboardNotifications];
 
}

-(void) createComment:(NSNotification*)sender
{
//    NSLog(@"Showing comment entry field");
    _commentAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _commentAccessoryView.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    _commentTextField = [[CLYCommentTextField alloc] initWithFrame:CGRectMake(8, 8, 223, 28)];
    _commentTextField.delegate = self;
    
    
    _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _commentButton.frame = CGRectMake(239, 8, 73, 28);
    NSAttributedString *attrButtonTitle = [[NSAttributedString alloc] initWithString:@"Comment" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ProximaNova-SemiBold" size:12.0], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [_commentButton setAttributedTitle:attrButtonTitle forState:UIControlStateNormal];
    _commentButton.layer.cornerRadius = 3.0;
    _commentButton.backgroundColor = [CLYApplicationConstants redButtonColor];
    [_commentButton addTarget:self action:@selector(saveComment) forControlEvents:UIControlEventTouchUpInside];
    
    [_commentAccessoryView addSubview:_commentTextField];
    [_commentAccessoryView addSubview:_commentButton];
    
    _commentPost = sender.object;
    
    _hiddenCommentTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _hiddenCommentTextField.delegate = self;
    [self.view.window addSubview:_hiddenCommentTextField];
    [_hiddenCommentTextField becomeFirstResponder];
    
}



-(void)saveComment
{
//    NSLog(@"Saving Comment: %@", _commentTextField.text);
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
    [[CLYPostsWebService sharedService] commentOnPost:_commentPost user:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] content:_commentTextField.text completionHandler:^{
        
        
    }];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self saveComment];
    return YES;
}

#pragma mark Keyboard stuff
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    [_commentTextField becomeFirstResponder];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    // Now add the view as an input accessory view to the selected textfield.
    if (textField != _commentTextField) {
        [textField setInputAccessoryView:_commentAccessoryView];
        
    }
    
}

-(void)compose
{
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"CreatePost" bundle:nil];
    UIViewController *vc = [post instantiateInitialViewController];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}

-(void)leftDrawerButtonPress:(id)sender{
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


#pragma mark TableView Datasource

- (void)configureCell:(UITableViewCell *)cell withObject:(CLYPost*)object
{
    CLYStreamCell *streamCell = (CLYStreamCell*)cell;
    [streamCell.headerView.userImage setImageWithURL:[NSURL URLWithString:object.user.avatar] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
    streamCell.headerView.userNameLabel.text = object.user.name;
    streamCell.headerView.userId = object.user.id;
    streamCell.headerView.timeLabel.text = [CLYHelperFunctions timePostedFormatted:object.created_at];

    if (object.photo && object.photo.length > 0 && ![object.photo isEqualToString:@"/photos/original/missing.png"]) {
        [streamCell.postImageView setImageWithURL:[NSURL URLWithString:object.photo]];
//        NSLog(@"Post HAS photo: %@",object.content);
        streamCell.heightConstraint.constant = 275.0;
        streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
        streamCell.headerView.userNameLabel.textColor = [UIColor whiteColor];
        streamCell.headerView.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0];
        streamCell.headerView.alpha = 0.7;
        streamCell.headerView.timeLabel.textColor = [UIColor whiteColor];
                     [self.view layoutIfNeeded];
    }else{
//                NSLog(@"Post no photo: %@",object.content);
        streamCell.heightConstraint.constant = 62.0;
        streamCell.headerView.userNameLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:15.0];
        streamCell.headerView.userNameLabel.textColor = [UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0];
        
        streamCell.headerView.timeLabel.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0];
        streamCell.headerView.backgroundColor = [UIColor clearColor];
        streamCell.headerView.alpha = 1.0;
                     [self.view layoutIfNeeded];
    }
    
    streamCell.postImageView.contentMode = UIViewContentModeScaleAspectFill;

    streamCell.footerView.postTextLabel.text = object.content;
    streamCell.footerView.communityId = object.community.id;
    streamCell.footerView.postId = object.id;
    streamCell.footerView.communityButton.hidden = NO;
    
    NSString *communityString = [NSString stringWithFormat:@"community: %@", object.community.name];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:communityString];
    
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5] range:NSMakeRange(0, attString.length)];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0] range:NSMakeRange(10, attString.length-10)];
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init ];
    pStyle.alignment = NSTextAlignmentLeft;
    [attString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, attString.length)];
    streamCell.footerView.communityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [streamCell.footerView.communityButton setAttributedTitle:attString forState:UIControlStateNormal];
    
    streamCell.footerView.favesButton.favesCount =[object.favesCount intValue];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLYPost *post = [self.dataSource objectAtIndexPath:indexPath];
   
    CGFloat header = 62.0;
    CGFloat image;
    
    if (post.photo && post.photo.length > 0 && ![post.photo isEqualToString:@"/photos/original/missing.png"]) {
        image = 275.0;
    }else{
        image = 0.0;
    }
    
    if(image>=62.0)
    {
        image = image-header;
    }
    
    CGFloat content = [self heightWithPost:post];
    CGFloat footer = 44.0;
    
    CGFloat height = header + image + content + footer;
//    NSLog(@"Height for row %i is %f", indexPath.row, height);
    
    
    return height;

}

-(CGFloat)heightForImage:(UIImage*)image inImageViewAspectFit:(UIImageView*)imageView
{
    if (!image) {
        return 0;
    }
    
    float imageRatio = image.size.width / image.size.height;
    
    float viewRatio = imageView.frame.size.width / imageView.frame.size.height;
    
    if(imageRatio < viewRatio)
    {
        return  imageView.frame.size.height;
    }
    else
    {
        float scale = imageView.frame.size.width / image.size.width;
        
        float height = scale * image.size.height;

        return  height;
    }
}

- (CGFloat) heightWithPost:(CLYPost *)post
{
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:13.5f]];
    [calculationView setText:post.content];
    CGSize expectedSize = [calculationView sizeThatFits:CGSizeMake(290.0f, MAXFLOAT)];
    
    return ceil(expectedSize.height) + 7.0f * 2; // 7 px padding top & bottom
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
//    NSLog(@"Compose tapped in CLYStreamViewController");
    UIStoryboard *postStoryBoard = [UIStoryboard storyboardWithName:@"Post" bundle:nil];
    CLYPostViewController *vc = [postStoryBoard instantiateInitialViewController];
    CLYPost *post = (CLYPost *)[self.dataSource objectAtIndexPath:indexPath];
    vc.postId = post.id;
    [self.navigationController pushViewController:vc animated:YES];
    

}

-(void)openCommunity:(NSNotification*)sender
{
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
    CLYCommunityViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYCommunityViewController"];
    vc.communityId = sender.object;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)openUser:(NSNotification*)sender
{
//    NSLog(@"StreamViewController openUser: %@",sender.object);
    
    [_commentTextField resignFirstResponder];
    [_hiddenCommentTextField resignFirstResponder];
    
    UIStoryboard *community = [UIStoryboard storyboardWithName:@"User" bundle:nil];
    CLYUserViewController *vc = [community instantiateViewControllerWithIdentifier:@"CLYUserViewController"];
    vc.userId = sender.object;
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
