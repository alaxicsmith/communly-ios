//
//  CLYStreamCell.m
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYStreamCell.h"

@implementation CLYStreamCell

-(void)awakeFromNib{
    
    self.headerView.timeLabel.font =[UIFont fontWithName:@"ProximaNova-Regular" size:12.5];
    self.headerView.userImage.layer.cornerRadius = 3.0;
    self.headerView.userImage.layer.masksToBounds = YES;
    self.footerView.postTextLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13.5];
    self.footerView.postTextLabel.textColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    line.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self.headerView addSubview:line];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse
{
    self.headerView.timeLabel.text = nil;
    self.headerView.userNameLabel.text = nil;
    self.headerView.userImage.image = nil;
    self.postImageView.image = nil;
    self.footerView.postTextLabel.text = nil;
}

@end
