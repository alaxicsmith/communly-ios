//
//  CLYStreamCellFooterView.m
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYStreamCellFooterView.h"
#import "CLYCommunityViewController.h"
#import "CLYPostsWebService.h"

@implementation CLYStreamCellFooterView


-(void)openCommunity:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_COMMUNITY" object:self.communityId];
}

- (IBAction)favePost:(id)sender {
//    NSLog(@"FAVES BUTTON TAPPED");
    [[CLYPostsWebService sharedService] markPostAsFavorite:self.postId forUser:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    self.favesButton.favesCount++;
    self.favesButton.enabled = NO;

}

- (IBAction)openComment:(id)sender {
//    NSLog(@"COMMENTS BUTTON TAPPED");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CREATE_COMMENT" object:self.postId];
}

@end
