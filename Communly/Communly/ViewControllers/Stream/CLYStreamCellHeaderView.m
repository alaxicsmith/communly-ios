//
//  CLYStreamCellHeaderView.m
//  Communly
//
//  Created by Chris Allwein on 3/11/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYStreamCellHeaderView.h"

@implementation CLYStreamCellHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)openUser:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_USER" object:self.userId];
}

@end
