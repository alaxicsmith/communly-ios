//
//  CLYViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYViewController.h"

@interface CLYViewController ()

@end

@implementation CLYViewController

-(void)awakeFromNib
{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.image = [UIImage imageNamed:@"hamburger"];
    leftDrawerButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    UIBarButtonItem *composeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"compose"] style:UIBarButtonItemStylePlain target:self action:@selector(compose)];
    composeButton.imageInsets = UIEdgeInsetsMake(0, -15, 0, 15);
    [self.navigationItem setRightBarButtonItem:composeButton animated:YES];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:52.0/255.0 blue:56.0/255.0 alpha:1.0];
    
    self.navigationController.navigationBar.translucent = NO;
    
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)compose
{
//    NSLog(@"Compose tapped in CLYViewController");
    UIStoryboard *post = [UIStoryboard storyboardWithName:@"CreatePost" bundle:nil];
    UIViewController *vc = [post instantiateInitialViewController];
    [self presentViewController:vc animated:YES completion:nil];

}

@end
