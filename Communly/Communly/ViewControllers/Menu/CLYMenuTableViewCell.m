//
//  CLYMenuTableViewCell.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYMenuTableViewCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (IBAction)toggleOpen:(id)sender {
    
//    NSLog(@"Tapped Section!");
    if ([self.parent canPerformAction:self.selectorForAction withSender:nil]) {
        [self.parent performSelector:self.selectorForAction withObject:nil afterDelay:0.0];
    }
    
}

- (void)awakeFromNib
{
    self.contentView.backgroundColor = [CLYApplicationConstants menuBackgroundColor];
    // Initialization code
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(toggleOpen:)];
    [self addGestureRecognizer:tapGesture];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
