//
//  CLYMenuViewController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYMenuViewController.h"
#import "CLYMenuSearchCell.h"
#import "CLYMenuSectionCell.h"
#import "CLYMenuUserCell.h"
#import "CLYMenuTableViewCell.h"
#import "UIViewController+MMDrawerController.h"
#import "CLYApplicationConstants.h"
#import "CLYSearchViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CLYUserViewController.h"
#import "CLYUserWebService.h"

@interface CLYMenuViewController ()

@end

@implementation CLYMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self reloadTable];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:@"UPDATE_USER" object:nil];
}

-(void)reloadTable
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.tableView.backgroundColor = [CLYApplicationConstants menuBackgroundColor];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 66.0;
    }
    return 44.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CLYMenuTableViewCell *cell;
    
    switch (section) {
        case 0:
            //Search Section
            cell = (CLYMenuSearchCell*)[tableView dequeueReusableCellWithIdentifier:@"search"];
            if (cell ==nil ) {
                cell = [[CLYMenuSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"search"];
            }
            cell.selectorForAction = @selector(openSearchSectionWithText:);
            break;
        case 1:
            //User Section
            cell = (CLYMenuUserCell*)[tableView dequeueReusableCellWithIdentifier:@"user"];
            if (cell ==nil ) {
                cell = [[CLYMenuUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"user"];
            }
            cell.selectorForAction = @selector(openUserSection);
            ((CLYMenuUserCell*)cell).userNameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userfullname"];
            [((CLYMenuUserCell*)cell).userImageView setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"new_user_photo"]];
         
            
            break;
        case 2:
            //Stream Section
            cell = (CLYMenuSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"section"];
            if (cell ==nil ) {
                cell = [[CLYMenuSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"section"];
            }
            cell.selectorForAction = @selector(openStreamSection);
            ((CLYMenuSectionCell*)cell).sectionNameLabel.text = @"Stream";
            ((CLYMenuSectionCell*)cell).sectionImageView.image = [UIImage imageNamed:@"stream"];
            ((CLYMenuSectionCell*)cell).sectionCountLabel.hidden = YES;
            ((CLYMenuSectionCell*)cell).sectionImageView.hidden = NO;
            break;
        case 3:
            //Explore Section
            cell = (CLYMenuSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"section"];
            if (cell ==nil ) {
                cell = [[CLYMenuSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"section"];
            }
            cell.selectorForAction = @selector(openExploreSection);
            ((CLYMenuSectionCell*)cell).sectionNameLabel.text = @"Explore";
            ((CLYMenuSectionCell*)cell).sectionImageView.image = [UIImage imageNamed:@"explore"];
            ((CLYMenuSectionCell*)cell).sectionCountLabel.hidden = YES;
            ((CLYMenuSectionCell*)cell).sectionImageView.hidden = NO;

            break;
        case 4:
            //Notifications Section
            cell = (CLYMenuSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"section"];
            if (cell ==nil ) {
                cell = [[CLYMenuSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"section"];
            }
            cell.selectorForAction = @selector(openNotificationsSection);
            ((CLYMenuSectionCell*)cell).sectionNameLabel.text = @"Notifications";
            ((CLYMenuSectionCell*)cell).sectionImageView.hidden = YES;
            ((CLYMenuSectionCell*)cell).sectionCountLabel.hidden = NO;
            ((CLYMenuSectionCell*)cell).sectionCountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[[CLYUserWebService sharedService].notifications count]];
            break;
        case 5:
            //Logout section
            cell = (CLYMenuSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"section"];
            if (cell ==nil ) {
                cell = [[CLYMenuSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"section"];
            }
            ((CLYMenuSectionCell*)cell).sectionNameLabel.text = @"Settings";
            ((CLYMenuSectionCell*)cell).sectionImageView.image = [UIImage imageNamed:@"settings"];
            ((CLYMenuSectionCell*)cell).sectionImageView.hidden = NO;
            ((CLYMenuSectionCell*)cell).sectionCountLabel.hidden = YES;
            cell.selectorForAction = @selector(openSettingsSection);
            break;
        case 6:
            //Logout section
            cell = (CLYMenuSectionCell*)[tableView dequeueReusableCellWithIdentifier:@"section"];
            if (cell ==nil ) {
                cell = [[CLYMenuSectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"section"];
            }
            ((CLYMenuSectionCell*)cell).sectionNameLabel.text = @"Invite Friends";
            ((CLYMenuSectionCell*)cell).sectionImageView.image = [UIImage imageNamed:@"inviteFriends"];
            ((CLYMenuSectionCell*)cell).sectionImageView.hidden = NO;
            ((CLYMenuSectionCell*)cell).sectionCountLabel.hidden = YES;
            cell.selectorForAction = @selector(openInviteFriendsSection);
            break;
        default:
            break;
    }
    
    cell.parent = self;
    
    return cell;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"Tapped Cell at %@", indexPath);
}

-(void) openUserSection
{
//    NSLog(@"Open User");
    UIStoryboard *user = [UIStoryboard storyboardWithName:@"User" bundle:nil];
    UINavigationController *vc = [user instantiateInitialViewController];
    ((CLYUserViewController*)vc.topViewController).userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
   
}

-(void) openSettingsSection
{
//    NSLog(@"Open Settings");
    UIStoryboard *settings = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *vc = [settings instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

-(void) openStreamSection
{
//    NSLog(@"Open Stream");
    UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Stream" bundle:nil];
    UIViewController *vc = [stream instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

}

-(void) openExploreSection
{
//    NSLog(@"Open Explore");
    UIStoryboard *communities = [UIStoryboard storyboardWithName:@"Explore" bundle:nil];
    UIViewController *vc = [communities instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

}

-(void) openNotificationsSection
{
//    NSLog(@"Open Notifications");
    UIStoryboard *profile = [UIStoryboard storyboardWithName:@"Notifications" bundle:nil];
    UIViewController *vc = [profile instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

}

-(void) openInviteFriendsSection
{
//    NSLog(@"Open InviteFriends");
    UIStoryboard *profile = [UIStoryboard storyboardWithName:@"InviteFriends" bundle:nil];
    UIViewController *vc = [profile instantiateInitialViewController];
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    
}

-(void) openSearchSectionWithText:(NSString*)text
{
//    NSLog(@"Open Search with term %@", text);
    UIStoryboard *search = [UIStoryboard storyboardWithName:@"Search" bundle:nil];
    UINavigationController *vc = [search instantiateInitialViewController];
    ((CLYSearchViewController*)vc.topViewController).searchString = text;
    [self.mm_drawerController setCenterViewController:vc];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    
}

@end
