//
//  CLYMenuSectionCell.h
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYMenuTableViewCell.h"

@interface CLYMenuSectionCell : CLYMenuTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sectionImageView;
@property (weak, nonatomic) IBOutlet UILabel *sectionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionCountLabel;

@end
