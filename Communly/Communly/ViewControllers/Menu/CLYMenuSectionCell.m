//
//  CLYMenuSectionCell.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYMenuSectionCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYMenuSectionCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.sectionNameLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:12.5];
    self.sectionCountLabel.backgroundColor = [CLYApplicationConstants applicationBackgroundColor];
    self.sectionCountLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:9];
    self.sectionCountLabel.layer.cornerRadius = 3.0;

}

@end
