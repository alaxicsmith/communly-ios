//
//  CLYMenuUserCell.h
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYMenuTableViewCell.h"

@interface CLYMenuUserCell : CLYMenuTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsImageView;

@property (nonatomic) SEL selectorForSettings;

@end
