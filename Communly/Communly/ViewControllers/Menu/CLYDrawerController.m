//
//  CLYDrawerController.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYDrawerController.h"

@implementation CLYDrawerController



-(void)awakeFromNib
{
//    UIStoryboard *menuBoard = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
//    UIViewController *menu = [menuBoard instantiateViewControllerWithIdentifier:@"menu"];
    UIStoryboard *streamBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *main = [streamBoard instantiateViewControllerWithIdentifier:@"loginnav"];

//    self.leftDrawerViewController = menu;
    self.centerViewController = main;
    
    [self setShowsShadow:NO];
    
    
    [self setRestorationIdentifier:@"MMDrawer"];
    [self setMaximumLeftDrawerWidth:220.0];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
//    [self
//     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
//         MMDrawerControllerDrawerVisualStateBlock block;
//         block = [[MMExampleDrawerVisualStateManager sharedManager]
//                  drawerVisualStateBlockForDrawerSide:drawerSide];
//         if(block){
//             block(drawerController, drawerSide, percentVisible);
//         }
//     }];
    
}




@end
