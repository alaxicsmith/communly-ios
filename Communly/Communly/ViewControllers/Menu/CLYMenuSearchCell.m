//
//  CLYMenuSearchCell.m
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYMenuSearchCell.h"
#import "CLYApplicationConstants.h"

@implementation CLYMenuSearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.contentView.backgroundColor = [CLYApplicationConstants menuBackgroundColor];
    self.searchBar.backgroundColor = [CLYApplicationConstants menuBackgroundColor];
    self.searchBar.delegate = self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 240.0f, 20.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
//    NSLog(@"searchBarSearchButtonClicked %@", searchBar.text);
    if ([self.parent canPerformAction:self.selectorForAction withSender:nil]) {
        [self.parent performSelector:self.selectorForAction withObject:searchBar.text afterDelay:0.0];
    }

    
}

@end
