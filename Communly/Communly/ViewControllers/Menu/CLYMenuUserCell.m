//
//  CLYMenuUserCell.m
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYMenuUserCell.h"
#import "UIViewController+MMDrawerController.h"

@implementation CLYMenuUserCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.userImageView.layer.cornerRadius = 3.0;
    self.userImageView.layer.masksToBounds = YES;
    self.userNameLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:12.5];
}

- (IBAction)toggleOpen:(id)sender {
  
//    NSLog(@"Tapped Section!");
    if ([self.parent canPerformAction:self.selectorForAction withSender:nil]) {
        [self.parent performSelector:self.selectorForAction withObject:nil afterDelay:0.0];
    }
    
}


@end
