//
//  CLYMenuTableViewCell.h
//  Communly
//
//  Created by Chris Allwein on 3/13/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYMenuViewController.h"

@interface CLYMenuTableViewCell : UITableViewCell

@property (nonatomic, strong) CLYMenuViewController *parent;
@property (nonatomic) SEL selectorForAction;

@end
