//
//  CLYMenuSearchCell.h
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLYMenuTableViewCell.h"

@interface CLYMenuSearchCell : CLYMenuTableViewCell<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
