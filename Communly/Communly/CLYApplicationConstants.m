//
//  CLYApplicationConstants.m
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYApplicationConstants.h"

@implementation CLYApplicationConstants

+(UIColor*) applicationBackgroundColor
{
    return [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
}

+(UIColor*) applicationTransparentBackgroundColor
{
    return [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.9];
}

+(UIColor*) redButtonColor
{
    return [UIColor colorWithRed:200.0/255.0 green:80.0/255.0 blue:72.0/255.0 alpha:1.0];
}

+(UIColor*) menuBackgroundColor
{
    return [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1.0];
}

+(NSString*) client_id
{
    //Prod
    return @"07afb9a3627e033a9888d5cdeb80e11d";

    //Test
    //    return @"b02a1d15820abee1b8a5e1a0f0415240";
}

+(NSString*) client_secret;
{
    //Prod
    return @"7a3ab697f1545e5a3b6fa34a24c9751b";
    
    //Test
    //    return @"2cf011af15cfffd6ce252226ebbe13fe";
}

@end
