//
//  CLYHelperFunctions.m
//  Communly
//
//  Created by Kelli Ireland on 2014/04/04.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYHelperFunctions.h"

@implementation CLYHelperFunctions

+(NSString *)timePostedFormatted:(NSDate *)date
{
    NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:date];
    
    //with the last 2 minutes, as now
    if(time<120)
    {
        return @"now";
    }
    
    //less than an hour ago, in minutes
    if(time<60*60)
    {
        return [NSString stringWithFormat:@"%i m",(int)time/60];
    }
    
    //less than a day ago, in jhours
    if(time<60*60*24)
    {
        return [NSString stringWithFormat:@"%i h",(int)time/60/60];
    }
    
    
    
    //less than a week ago, in days
    if(time<60*60*24*7)
    {
        return [NSString stringWithFormat:@"%i d",(int)time/(60*60*24)];
    }
    
    
    //less than 6 weeks ago, in weeks
    if(time<60*60*24*7*6)
    {
        return [NSString stringWithFormat:@"%i w",(int)time/(60*60*24*7)];
    }
    
    
    //over 6 weeks ago, in months
    if(time>=60*60*24*7*6)
    {
        return [NSString stringWithFormat:@"%i months",(int)time/(60*60*24*30)];
    }
    
    
    
    //Should never reach here
    return @"";
}

@end
