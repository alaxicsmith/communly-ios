//
//  CLYHelperFunctions.h
//  Communly
//
//  Created by Kelli Ireland on 2014/04/04.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLYHelperFunctions : NSObject

+(NSString *)timePostedFormatted:(NSDate *)date;

@end
