//
//  CLYCommentTextField.m
//  Communly
//
//  Created by Chris Allwein on 5/5/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCommentTextField.h"
#import "CLYApplicationConstants.h"

@implementation CLYCommentTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.layer.borderColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0].CGColor;
        self.layer.borderWidth = 1.0;
        self.layer.cornerRadius = 3.0;
        self.font = [UIFont fontWithName:@"ProximaNova-Regular" size:12.0];
        self.tintColor = [CLYApplicationConstants applicationBackgroundColor];

    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"join the conversation..." attributes:@{ NSForegroundColorAttributeName : [CLYApplicationConstants applicationBackgroundColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0] }];
    self.attributedPlaceholder = attrString;
}


// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 15, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 15, 0);
}

@end
