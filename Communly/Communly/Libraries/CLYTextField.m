//
//  CLYTextField.m
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYTextField.h"

@implementation CLYTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code


    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
//    self.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:68.0/255.0 blue:68.0/255.0 alpha:1.0];
    self.layer.cornerRadius = 3.0;
    if (_useDarkTheme) {
        NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5] }];
        
        [self setAttributedPlaceholder:placeholder];
        
    }else{
        NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:17.5] }];
        
        [self setAttributedPlaceholder:placeholder];
        
    }
    
    

}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 25, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 25, 0);
}

- (CGRect) leftViewRectForBounds:(CGRect)bounds  {
    
    CGRect textRect = [super leftViewRectForBounds:bounds];
    textRect.origin.x -= 10;
    return textRect;
}

@end
