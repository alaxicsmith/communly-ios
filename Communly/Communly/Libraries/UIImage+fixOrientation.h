//
//  UIImage+fixOrientation.h
//  truckitokc
//
//  Created by Chris Allwein on 6/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
