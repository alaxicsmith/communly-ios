//
//  CLYFavesButton.h
//  Communly
//
//  Created by Chris Allwein on 7/2/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYFavesButton : UIButton

@property (nonatomic) int favesCount;

@end
