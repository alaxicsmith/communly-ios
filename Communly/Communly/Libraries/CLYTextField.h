//
//  CLYTextField.h
//  Communly
//
//  Created by Chris Allwein on 3/18/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLYTextField : UITextField
@property(nonatomic) BOOL useDarkTheme;
@end
