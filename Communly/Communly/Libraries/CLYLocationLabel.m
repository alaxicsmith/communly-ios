//
//  CLYLocationLabel.m
//  Communly
//
//  Created by Chris Allwein on 7/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYLocationLabel.h"

@implementation CLYLocationLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIEdgeInsets insets = {0, 15, 0, 0};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    
}


@end
