//
//  CLYFavesButton.m
//  Communly
//
//  Created by Chris Allwein on 7/2/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYFavesButton.h"

@implementation CLYFavesButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
        self.adjustsImageWhenDisabled = NO;
    _favesCount = 0;
    [self updateButtonTitle];
    
}

-(void)setFavesCount:(int)favesCount
{
    _favesCount = favesCount;
    [self updateButtonTitle];

}

-(void) updateButtonTitle
{
    NSString *favesString = [NSString stringWithFormat:@"%i Faves", _favesCount ];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:favesString];
    
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ProximaNova-SemiBold" size:12] range:NSMakeRange(0, attString.length)];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, attString.length)];
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init ];
    pStyle.alignment = NSTextAlignmentLeft;
    [attString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, attString.length)];
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [self setAttributedTitle:attString forState:UIControlStateNormal];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
