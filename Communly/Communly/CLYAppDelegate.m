//
//  CLYAppDelegate.m
//  Communly
//
//  Created by Chris Allwein on 3/10/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYAppDelegate.h"
#import "CLYAsyncDataStore.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFOAuth2Client.h"
#import "CLYUserWebService.h"

@implementation CLYAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSLog(@"didFinishLaunchingWithOptions");
   
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0]}];
  
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
//     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0]} forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor redColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetLogin) name:@"NEEDS_LOGIN" object:nil];

    [[CLYUserWebService sharedService] getNotificationsForCurrentUserWithCompletionHandler:^(NSArray *notifications) {}];

    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-568h"]];
    [self.window.rootViewController.view addSubview:iv];
    [UIView animateWithDuration:2.5 animations:^{
        iv.alpha = 0.0;
    } completion:^(BOOL finished) {
        [iv removeFromSuperview];
    }];

    
//   [self resetLogin];
    
    // Override point for customization after application launch.
    return YES;
}

-(void) resetLogin
{
//    [AFOAuthCredential deleteCredentialWithIdentifier:@"communly-testing.herokuapp.com"];
    [AFOAuthCredential deleteCredentialWithIdentifier:@"communly.herokuapp.com"];
    
    UIStoryboard *stream = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *vc = [stream instantiateInitialViewController];
    self.window.rootViewController = vc;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
//    NSLog(@"applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

  
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    NSLog(@"applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
//    NSLog(@"applicationWillEnterForeground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
   
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//    NSLog(@"applicationDidBecomeActive");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[CLYUserWebService sharedService] getNotificationsForCurrentUserWithCompletionHandler:^(NSArray *notifications) {}];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
}


- (void)applicationWillTerminate:(UIApplication *)application
{
//    NSLog(@"");
    // Saves changes in the application's managed object context before the application terminates.
    [[CLYAsyncDataStore sharedStore] saveContext];
}



@end
