//
//  CLYCommunity.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ModelObject.h"

@class CLYPost, CLYUser;

@interface CLYCommunity : ModelObject

@property (nonatomic, retain) NSString * about;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * avatar;
@property (nonatomic, retain) NSNumber * communityMemberCount;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSNumber * communityVerified;
@property (nonatomic, retain) NSString * background;
@property (nonatomic, retain) NSOrderedSet *posts;
@property (nonatomic, retain) NSSet *users;

- (void)loadFromDictionary:(NSDictionary *)dictionary;
+ (CLYCommunity *)findOrCreateCommunityWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;
+ (CLYCommunity *) communityWithResponseObject:(id)responseObject;

@end

@interface CLYCommunity (CoreDataGeneratedAccessors)

- (void)insertObject:(CLYPost *)value inPostsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPostsAtIndex:(NSUInteger)idx;
- (void)insertPosts:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePostsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPostsAtIndex:(NSUInteger)idx withObject:(CLYPost *)value;
- (void)replacePostsAtIndexes:(NSIndexSet *)indexes withPosts:(NSArray *)values;
- (void)addPostsObject:(CLYPost *)value;
- (void)removePostsObject:(CLYPost *)value;
- (void)addPosts:(NSOrderedSet *)values;
- (void)removePosts:(NSOrderedSet *)values;
- (void)addUsersObject:(CLYUser *)value;
- (void)removeUsersObject:(CLYUser *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
