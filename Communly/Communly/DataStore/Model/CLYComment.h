//
//  CLYComment.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ModelObject.h"

@class CLYPost, CLYUser;

@interface CLYComment : ModelObject

@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) CLYPost *post;
@property (nonatomic, retain) CLYUser *user;

- (void)loadFromDictionary:(NSDictionary *)dictionary;
+ (CLYComment *)findOrCreateCommentWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;
+ (CLYComment *) commentWithResponseObject:(id)responseObject;

@end
