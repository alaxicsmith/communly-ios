//
//  CLYComment.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYComment.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYAsyncDataStore.h"

@implementation CLYComment

@dynamic created_at;
@dynamic content;
@dynamic id;
@dynamic post;
@dynamic user;

- (void)loadFromDictionary:(NSDictionary *)dictionary
{
    [self safeSetValuesForKeysWithDictionary:dictionary];

    if (dictionary[@"user"]) {
        self.user = [CLYUser userWithResponseObject:dictionary[@"user"]];
    }
    
    //    "comments":[{"id":113,"created_at":"2014-01-30T09:21:00.193Z","content":"Turn up","user":{"id":4,"name":"Neil Parikh","username":"neil","avatar":"http://s3.amazonaws.com/communly/users/avatars/000/000/004/original/Photo_on_2013-01-22_at_23.06__2.jpg?1388907770"}}]}
}

+ (CLYComment *)findOrCreateCommentWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"id = %@", identifier];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }
    if (result.lastObject) {
        return result.lastObject;
    } else {
        CLYComment *comment = [self insertNewObjectIntoContext:context];
        comment.id = identifier;
        return comment;
    }
    
}

+ (CLYComment *) commentWithResponseObject:(id)responseObject
{
    if (![responseObject isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    NSString *commentId = [responseObject[@"id"] stringValue];
    
    if (!commentId || [commentId isEqualToString:@""]) {
        return nil;
    }
    
    CLYComment *comment = [CLYComment findOrCreateCommentWithIdentifier:commentId inContext:[CLYAsyncDataStore sharedStore].getBackgroundManagedObjectContext];
    [comment loadFromDictionary:responseObject];
    return comment;
    
}
@end
