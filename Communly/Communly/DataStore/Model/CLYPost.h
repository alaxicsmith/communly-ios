//
//  CLYPost.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ModelObject.h"

@class CLYComment, CLYCommunity, CLYUser;

@interface CLYPost : ModelObject

@property (nonatomic, retain) NSNumber * favesCount;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSOrderedSet *comments;
@property (nonatomic, retain) CLYCommunity *community;
@property (nonatomic, retain) CLYUser *user;

- (void)loadFromDictionary:(NSDictionary *)dictionary;
+ (CLYPost *)findOrCreatePostWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;
+ (CLYPost *) postWithResponseObject:(id)responseObject;

@end

@interface CLYPost (CoreDataGeneratedAccessors)

- (void)insertObject:(CLYComment *)value inCommentsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCommentsAtIndex:(NSUInteger)idx;
- (void)insertComments:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCommentsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCommentsAtIndex:(NSUInteger)idx withObject:(CLYComment *)value;
- (void)replaceCommentsAtIndexes:(NSIndexSet *)indexes withComments:(NSArray *)values;
- (void)addCommentsObject:(CLYComment *)value;
- (void)removeCommentsObject:(CLYComment *)value;
- (void)addComments:(NSOrderedSet *)values;
- (void)removeComments:(NSOrderedSet *)values;
@end
