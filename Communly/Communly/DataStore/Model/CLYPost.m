//
//  CLYPost.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPost.h"
#import "CLYComment.h"
#import "CLYCommunity.h"
#import "CLYUser.h"
#import "CLYAsyncDataStore.h"


@implementation CLYPost

@dynamic favesCount;
@dynamic created_at;
@dynamic id;
@dynamic photo;
@dynamic content;
@dynamic comments;
@dynamic community;
@dynamic user;

- (void)loadFromDictionary:(NSDictionary *)dictionary
{
    [self safeSetValuesForKeysWithDictionary:dictionary];
    
    if (dictionary[@"user"]) {
        self.user = [CLYUser userWithResponseObject:dictionary[@"user"]];
    }
    if (dictionary[@"community"]) {
        self.community = [CLYCommunity communityWithResponseObject:dictionary[@"community"]];
    }
    
    if (dictionary[@"comments"]) {
        NSMutableOrderedSet *set = [[NSMutableOrderedSet alloc] init];
        
        if ([dictionary[@"comments"] isKindOfClass:[NSDictionary class]] && dictionary[@"comments"][@"comments"]) {
            for (NSDictionary *comment in dictionary[@"comments"][@"comments"]) {
                CLYComment *c = [CLYComment commentWithResponseObject:comment];
                if (c)
                {
                    [set addObject:c];
                }
            }

        }else{
            for (NSDictionary *comment in dictionary[@"comments"]) {
                CLYComment *c = [CLYComment commentWithResponseObject:comment];
                if (c)
                {
                    [set addObject:c];
                }
            }
        }
        self.comments = set;
    }
    
    if (dictionary[@"favorites"]) {
        self.favesCount = [NSNumber numberWithInt:[dictionary[@"favorites"] intValue]];
    }

    
    
    
    //{"id":474,"photo":"http://s3.amazonaws.com/communly/posts/photos/000/000/474/original/_20140129_123720.JPG?1391027911","content":"the work doesn't stop. improvements and fixes on the way. ","created_at":"2014-01-29T20:38:31.926Z","user":{"name":"Alaxic Smith","username":"alaxic","avatar":"http://s3.amazonaws.com/communly/users/avatars/000/000/001/original/square.jpg?1388907767","id":1},"comments":[{"id":113,"created_at":"2014-01-30T09:21:00.193Z","content":"Turn up","user":{"id":4,"name":"Neil Parikh","username":"neil","avatar":"http://s3.amazonaws.com/communly/users/avatars/000/000/004/original/Photo_on_2013-01-22_at_23.06__2.jpg?1388907770"}}]}
    
}

+ (CLYPost *) postWithResponseObject:(id)responseObject
{
    if (![responseObject isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    NSString *postId = [responseObject[@"id"] stringValue];
    
    if (!postId || [postId isEqualToString:@""]) {
        return nil;
    }
    
    CLYPost *post = [CLYPost findOrCreatePostWithIdentifier:postId inContext:[CLYAsyncDataStore sharedStore].getBackgroundManagedObjectContext];
    [post loadFromDictionary:responseObject];
    return post;
    
}

+ (CLYPost *)findOrCreatePostWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"id = %@", identifier];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }
    if (result.lastObject) {
        return result.lastObject;
    } else {
        CLYPost *post = [self insertNewObjectIntoContext:context];
        post.id = identifier;
        return post;
    }
    
}


@end
