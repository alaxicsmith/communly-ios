//
//  CLYUser.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ModelObject.h"

@class CLYComment, CLYCommunity, CLYPost, CLYUser;

@interface CLYUser : ModelObject

@property (nonatomic, retain) NSString * about;
@property (nonatomic, retain) NSNumber * userFollowersCount;
@property (nonatomic, retain) NSNumber * userFollowingCount;
@property (nonatomic, retain) NSNumber * userCommunitiesCount;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * avatar;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * background;
@property (nonatomic, retain) NSOrderedSet *comments;
@property (nonatomic, retain) NSSet *communities;
@property (nonatomic, retain) NSSet *followers;
@property (nonatomic, retain) NSSet *following;
@property (nonatomic, retain) NSOrderedSet *posts;

- (void)loadFromDictionary:(NSDictionary *)dictionary;
+ (CLYUser *)findOrCreateUserWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;
+ (CLYUser *) userWithResponseObject:(id)responseObject;

@end

@interface CLYUser (CoreDataGeneratedAccessors)

- (void)insertObject:(CLYComment *)value inCommentsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCommentsAtIndex:(NSUInteger)idx;
- (void)insertComments:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCommentsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCommentsAtIndex:(NSUInteger)idx withObject:(CLYComment *)value;
- (void)replaceCommentsAtIndexes:(NSIndexSet *)indexes withComments:(NSArray *)values;
- (void)addCommentsObject:(CLYComment *)value;
- (void)removeCommentsObject:(CLYComment *)value;
- (void)addComments:(NSOrderedSet *)values;
- (void)removeComments:(NSOrderedSet *)values;
- (void)addCommunitiesObject:(CLYCommunity *)value;
- (void)removeCommunitiesObject:(CLYCommunity *)value;
- (void)addCommunities:(NSSet *)values;
- (void)removeCommunities:(NSSet *)values;

- (void)addFollowersObject:(CLYUser *)value;
- (void)removeFollowersObject:(CLYUser *)value;
- (void)addFollowers:(NSSet *)values;
- (void)removeFollowers:(NSSet *)values;

- (void)addFollowingObject:(CLYUser *)value;
- (void)removeFollowingObject:(CLYUser *)value;
- (void)addFollowing:(NSSet *)values;
- (void)removeFollowing:(NSSet *)values;

- (void)insertObject:(CLYPost *)value inPostsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPostsAtIndex:(NSUInteger)idx;
- (void)insertPosts:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePostsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPostsAtIndex:(NSUInteger)idx withObject:(CLYPost *)value;
- (void)replacePostsAtIndexes:(NSIndexSet *)indexes withPosts:(NSArray *)values;
- (void)addPostsObject:(CLYPost *)value;
- (void)removePostsObject:(CLYPost *)value;
- (void)addPosts:(NSOrderedSet *)values;
- (void)removePosts:(NSOrderedSet *)values;
@end
