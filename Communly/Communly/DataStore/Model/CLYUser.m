//
//  CLYUser.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUser.h"
#import "CLYComment.h"
#import "CLYCommunity.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYAsyncDataStore.h"

@implementation CLYUser
@dynamic userCommunitiesCount;
@dynamic about;
@dynamic userFollowersCount;
@dynamic userFollowingCount;
@dynamic name;
@dynamic id;
@dynamic avatar;
@dynamic location;
@dynamic username;
@dynamic email;
@dynamic background;
@dynamic comments;
@dynamic communities;
@dynamic followers;
@dynamic following;
@dynamic posts;


- (void)loadFromDictionary:(NSDictionary *)dictionary
{
        [self safeSetValuesForKeysWithDictionary:dictionary];
    
    if (self.background && ![[self.background substringToIndex:4] isEqualToString:@"http"]) {
        self.background = [NSString stringWithFormat:@"http://communly.herokuapp.com%@",self.background];
    }
    
    if (self.avatar && ![[self.avatar substringToIndex:4] isEqualToString:@"http"]) {
        self.avatar = [NSString stringWithFormat:@"http://communly.herokuapp.com%@",self.avatar];
    }
    
    if ([dictionary objectForKey:@"followers"]) {
        self.userFollowersCount = [NSNumber numberWithInteger:[[dictionary objectForKey:@"followers"] integerValue]];
    }

    if ([dictionary objectForKey:@"community_count"]) {
        self.userCommunitiesCount = [NSNumber numberWithInteger:[[dictionary objectForKey:@"community_count"] integerValue]];
    }

    if ([dictionary objectForKey:@"following"]) {
        self.userFollowingCount = [NSNumber numberWithInteger:[[dictionary objectForKey:@"following"] integerValue]];
    }

    
    
    //    {"id":1,"email":"alaxicsmith@gmail.com","name":"Alaxic Smith","username":"alaxic","location":"San Francisco, CA","about":"starting a new movement at communly - fullstack dev \u0026 UI/UX designer - hit me up alaxic@communly.herokuapp.com test","avatar":"http://s3.amazonaws.com/communly/users/avatars/000/000/001/original/square.jpg?1388907767","background":"http://communly.s3.amazonaws.com/users/backgrounds/000/000/001/original/Screen_Shot_2013-11-05_at_2.54.43_AM.png?1388908310"}
}

+ (CLYUser *)findOrCreateUserWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"id = %@", identifier];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }
    if (result.lastObject) {
        return result.lastObject;
    } else {
        CLYUser *user = [self insertNewObjectIntoContext:context];
        user.id = identifier;
        return user;
    }
    
}

+ (CLYUser *) userWithResponseObject:(id)responseObject
{
    if (![responseObject isKindOfClass:[NSDictionary class]]) {
        return nil;
    }

    NSDictionary *userDict = responseObject;
    
    //TODO: Hack because of weird bundling on user endpoint.
    if (responseObject[@"user"]) {
        userDict = responseObject[@"user"];
    }
    
    NSString *userId = [userDict[@"id"] stringValue];
    
    if (!userId || [userId isEqualToString:@""]) {
        userId = [userDict[@"user_id"] stringValue];
        if (!userId || [userId isEqualToString:@""]) {
            return nil;
        }
    }
    
    CLYUser *user = [CLYUser findOrCreateUserWithIdentifier:userId inContext:[CLYAsyncDataStore sharedStore].getBackgroundManagedObjectContext];
    [user loadFromDictionary:userDict];
    
   //TODO: Additional Hack because of weird bundling on user endpoint.
    if ([responseObject objectForKey:@"followers"]) {
        user.userFollowersCount = [NSNumber numberWithInteger:[[responseObject objectForKey:@"followers"] integerValue]];
    }
    
    if ([responseObject objectForKey:@"following"]) {
        user.userFollowingCount = [NSNumber numberWithInteger:[[responseObject objectForKey:@"following"] integerValue]];
    }
    
    if ([responseObject objectForKey:@"community_count"]) {
        user.userCommunitiesCount = [NSNumber numberWithInteger:[[responseObject objectForKey:@"community_count"] integerValue]];
    }

    
    
    return user;
    
}
@end
