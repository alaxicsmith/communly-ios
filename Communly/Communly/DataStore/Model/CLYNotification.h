//
//  CLYNotification.h
//  Communly
//
//  Created by Chris Allwein on 5/7/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLYUser.h"


@interface CLYNotification : NSObject

@property(nonatomic,strong) CLYUser *user;
@property(nonatomic,strong) NSDate *notificationDate;
@property(nonatomic,strong) NSAttributedString *notificationContent;
@property(nonatomic,strong) UIImage *notificationTypeImage;
@property(nonatomic,strong) NSString  *objectId;
@property(nonatomic,strong) NSString  *notificationAction;

@property(nonatomic,strong) NSString  *postId;
@property(nonatomic,strong) NSString  *userId;
@property(nonatomic,strong) NSString  *communityId;


@end
