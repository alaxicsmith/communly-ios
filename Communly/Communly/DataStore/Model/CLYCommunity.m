//
//  CLYCommunity.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCommunity.h"
#import "CLYPost.h"
#import "CLYUser.h"
#import "CLYAsyncDataStore.h"


@implementation CLYCommunity

@dynamic about;
@dynamic id;
@dynamic avatar;
@dynamic communityMemberCount;
@dynamic name;
@dynamic category;
@dynamic website;
@dynamic communityVerified;
@dynamic background;
@dynamic posts;
@dynamic users;


- (void)loadFromDictionary:(NSDictionary *)dictionary
{
        [self safeSetValuesForKeysWithDictionary:dictionary];

    if (dictionary[@"members"]) {
        if ([dictionary[@"members"] isKindOfClass:[NSArray class]]) {
            self.communityMemberCount = [NSNumber numberWithInt:(int)[dictionary[@"members"] count]];
        }else{
            self.communityMemberCount =dictionary[@"members"];
        }

    }

    if (dictionary[@"member_count"]) {
        self.communityMemberCount =dictionary[@"member_count"];
    }
    
    if (self.background && ![[self.background substringToIndex:4] isEqualToString:@"http"]) {
        self.background = [NSString stringWithFormat:@"http://communly.herokuapp.com%@",self.background];
    }
    
    if (self.avatar && ![[self.avatar substringToIndex:4] isEqualToString:@"http"]) {
        self.avatar = [NSString stringWithFormat:@"http://communly.herokuapp.com%@",self.avatar];
    }
    
    
    //    {"id":17,"name":"Communly","about":"the official Communly community. we're here to listen to you. hit us up at hi@communly.herokuapp.com","user_id":"1","website":"http://communly.herokuapp.com","category":"Startup","avatar":"http://s3.amazonaws.com/communly/communities/avatars/000/000/017/original/home-logo.png?1388908743","background":"http://communly.s3.amazonaws.com/communities/backgrounds/000/000/017/original/bg.jpg?1388908922"}
}

+ (CLYCommunity *)findOrCreateCommunityWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"id = %@", identifier];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }
    if (result.lastObject) {
        return result.lastObject;
    } else {
        CLYCommunity *community = [self insertNewObjectIntoContext:context];
        community.id = identifier;
        return community;
    }
    
}


+ (CLYCommunity *) communityWithResponseObject:(id)responseObject
{
    if (![responseObject isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    NSString *communityId = [responseObject[@"id"] stringValue];
    
    if (!communityId || [communityId isEqualToString:@""]) {
        return nil;
    }
    
    CLYCommunity *community = [CLYCommunity findOrCreateCommunityWithIdentifier:communityId inContext:[CLYAsyncDataStore sharedStore].getBackgroundManagedObjectContext];
    [community loadFromDictionary:responseObject];
    return community;
    
}

@end
