//
//  CLYUserWebService.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYUserWebService.h"
#import "CLYSessionManager.h"
#import "CLYUser.h"
#import "CLYApplicationConstants.h"
#import "CLYNotification.h"
#import "AFHTTPRequestOperationManager.h"

@implementation CLYUserWebService

+ (CLYUserWebService *)sharedService
{
    static CLYUserWebService *sharedService = nil;
    if(!sharedService)
        sharedService = [[super allocWithZone:nil] init];
    
    return sharedService;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedService];
}

-(id)init
{
    self = [super init];
    _notifications = [NSArray array];
    
    return self;
}

-(void)getUserWithId:(NSString*)userId completionHandler:(void (^)(CLYUser *user))callback
{
    NSString *urlString = [NSString stringWithFormat:@"users/%@",userId];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        CLYUser *user = [CLYUser userWithResponseObject:responseObject];
        callback(user);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
}


-(void)getCurrentUserWithCompletionHandler:(void (^)(CLYUser *user))callback
{
    NSString *urlString = @"users/me.json";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//                NSLog(@"Response: %@", responseObject);
        CLYUser *user = [CLYUser userWithResponseObject:responseObject];
        callback(user);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}


-(void)getFeaturedUsersWithCompletionHandler:(void (^)(NSArray *users))callback
{
   
    NSString *urlString = @"featured_users/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *users = [NSMutableArray array];
        for (id object in responseObject) {
            CLYUser *user = [CLYUser userWithResponseObject:object];
            [users addObject:user];
        }
       
        callback(users);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
}

-(void)getNotificationsForCurrentUserWithCompletionHandler:(void (^)(NSArray *notifications))callback
{
    NSString *urlString = @"users/me/notifications/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//                NSLog(@"Response: %@", responseObject);
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        NSMutableArray *notifications = [NSMutableArray array];
        for (id object in responseObject) {
            CLYNotification *n = [[CLYNotification alloc] init];
            
            CLYUser *user = [CLYUser userWithResponseObject:[object objectForKey:@"from"]];
            n.user = user;
            n.notificationDate = [dateFormatter dateFromString:[object objectForKey:@"created_at"]];
            
            //TODO: Properly Parse
            NSString *action = [[object objectForKey:@"object"] objectForKey:@"type"];
            n.notificationAction = action;
            
            if([action isEqualToString:@"Favorite"])
            {
                n.notificationTypeImage = [UIImage imageNamed:@"notification_favorite"];
                NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"favorited %@", [[object objectForKey:@"object"] objectForKey:@"content"]]];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:12.5]} range:NSMakeRange(0, 10)];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5]} range:NSMakeRange(10, content.length-10)];
                n.notificationContent = content;
                n.postId = [[object objectForKey:@"object"] objectForKey:@"post_id"];
                
            }else if ([action isEqualToString:@"Comment"]){
                n.notificationTypeImage = [UIImage imageNamed:@"notification_comment"];
                NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"commented %@", [[object objectForKey:@"object"] objectForKey:@"comment"]]];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:12.5]} range:NSMakeRange(0, 10)];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5]} range:NSMakeRange(10, content.length-10)];
                n.notificationContent = content;
                n.postId = [[object objectForKey:@"object"] objectForKey:@"post_id"];
                
            }else if ([action isEqualToString:@"User"]){
                n.notificationTypeImage = [UIImage imageNamed:@"notification_friend"];
                NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"followed %@", [[object objectForKey:@"object"] objectForKey:@"name"]]];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:12.5]} range:NSMakeRange(0, 9)];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5]} range:NSMakeRange(9, content.length-9)];
                n.userId = [[object objectForKey:@"object"] objectForKey:@"user_id"];
                n.notificationContent = content;
                
                
            }else if ([action isEqualToString:@"Community"]){
                n.notificationTypeImage = [UIImage imageNamed:@"notification_join"];
                NSString *name = [[object objectForKey:@"object"] objectForKey:@"name"];
                NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"joined your %@ community", name]];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:12.5]} range:NSMakeRange(0, 12)];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:12.5]} range:NSMakeRange(12, name.length)];
                [content addAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Light" size:12.5]} range:NSMakeRange(content.length-10, 10)];
                n.communityId = [[object objectForKey:@"object"] objectForKey:@"object_id"];
                n.notificationContent = content;
                
                
            }
            
            
            
            
            
            [notifications addObject:n];
        }
        
        _notifications = notifications;
        
        callback(notifications);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];

}

-(void)putUser:(CLYUser*)user avatar:(UIImage*)avatar background:(UIImage*)background completionHandler:(void (^)(CLYUser *user))callback
{
    NSMutableDictionary *userdict = [NSMutableDictionary dictionary];
    [userdict setObject:user.location forKey:@"location"];
    [userdict setObject:user.about forKey:@"about"];
    [userdict setObject:user.name forKey:@"name"];
    
    if (avatar) {
        NSString *imageData = [UIImagePNGRepresentation(avatar) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        imageData = [NSString stringWithFormat:@"%@%@",@"data:image/jpeg;base64,",imageData];

        [userdict setValue:imageData forKey:@"avatar"];
    }
    
    if (background) {
        NSString *imageData = [UIImagePNGRepresentation(background) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        imageData = [NSString stringWithFormat:@"%@%@",@"data:image/jpeg;base64,",imageData];

        [userdict setValue:imageData forKey:@"background"];
    }
    
    NSDictionary *params =  @{ @"user":userdict};
    
    
//    NSLog(@"JSON: %@",      [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] );
    
    [[CLYSessionManager sharedManager] PUT:@"users/me/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Success Response: %@", responseObject);
        CLYUser *user = [CLYUser userWithResponseObject:responseObject];
        callback(user);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        NSLog(@"Error: %@ ***** %@", task.response, error);
    }];

}

-(void)createUserWithName:(NSString*)name emailAddress:(NSString*)email username:(NSString*)username password:(NSString*)password completionHandler:(void (^)(NSObject *object))callback
{
    NSString *urlString = [NSString stringWithFormat:@"users/"];
    
    NSDictionary *user = @{@"name":name,@"email":email,@"username":username,@"password":password};

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:user forKey:@"user"];
    [dict setValue:[CLYApplicationConstants client_id] forKey:@"client_id"];
    [dict setValue:[CLYApplicationConstants client_secret] forKey:@"client_secret"];
    
    [[CLYSessionManager sharedManager] POST:urlString parameters:dict success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Success Response: %@", responseObject);
        CLYUser *user = [CLYUser userWithResponseObject:responseObject];
        if (user) {
            callback(user);
        }else{
            callback(responseObject);
        }

        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
    
    
    
    
    
}

-(void)followUserWithId:(NSString*)userId
{
    NSString *urlString = [NSString stringWithFormat:@"users/me/neighbor/%@/",userId];
    [[CLYSessionManager sharedManager] PUT:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Succesfully followed User: %@", userId);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Follow ERROR: %@", error);
    }];
    
}

-(void)unfollowUserWithId:(NSString*)userId
{
    NSString *urlString = [NSString stringWithFormat:@"users/me/neighbor/%@/",userId];
    [[CLYSessionManager sharedManager] DELETE:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Succesfully unfollowed User: %@", userId);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Unfollow ERROR: %@", error);
    }];
    
}


@end
