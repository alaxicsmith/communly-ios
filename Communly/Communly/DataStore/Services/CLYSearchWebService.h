//
//  CLYSearchWebService.h
//  Communly
//
//  Created by Chris Allwein on 4/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLYUser;
@class CLYCommunity;
@class CLYPost;

@interface CLYSearchWebService : NSObject

+ (CLYSearchWebService *)sharedService;

-(void)getSearchResultsWithQuery:(NSString*)searchString completionHandler:(void (^)(NSDictionary *results))callback;

@end
