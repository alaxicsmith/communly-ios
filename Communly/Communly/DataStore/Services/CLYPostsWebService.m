//
//  CLYPostsWebService.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYPostsWebService.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFNetworking.h"
#import "CLYAsyncDataStore.h"
#import "CLYSessionManager.h"
#import "CLYPost.h"
#import "CLYComment.h"

@interface CLYPostsWebService ()

@property (nonatomic, strong) NSURLSessionDataTask *task;
@end

@implementation CLYPostsWebService

+ (CLYPostsWebService *)sharedService
{
    static CLYPostsWebService *sharedService = nil;
    if(!sharedService)
        sharedService = [[super allocWithZone:nil] init];
    
    return sharedService;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedService];
}


-(void) createPostWithContent:(NSString *)content withImage:(UIImage *)image forCommunityWithId:(NSString*)communityId byUserWithId:(NSString*)userId
{
    NSString *urlString = [NSString stringWithFormat:@"posts/"];

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:content forKey:@"content"];
    if (image) {
        NSString *imageData = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        imageData = [NSString stringWithFormat:@"%@%@",@"data:image/jpeg;base64,",imageData];
        [dict setValue:imageData forKey:@"photo"];
    }

    [dict setValue:userId forKey:@"user_id"];
    [dict setValue:communityId forKey:@"community_id"];
    
    NSDictionary *postDict = @{@"post":dict};

//    NSLog(@"JSON: %@",      [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] );
    [[CLYSessionManager sharedManager] POST:urlString parameters:postDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"Success Response: %@", responseObject);
        //TODO: Refresh page they were on?
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        NSLog(@"ERROR: %@", error);
    }];

    
}

-(void)getPostWithId:(NSString*)postId completionHandler:(void (^)(CLYPost *post))callback
{
    NSString *urlString = [NSString stringWithFormat:@"posts/%@",postId];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        CLYPost *post = [CLYPost postWithResponseObject:responseObject];
        callback(post);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}

-(void)getPostsWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *posts))callback
{
    NSString *urlString = [NSString stringWithFormat:@"users/%@/posts/",userId];
    
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *posts = [NSMutableArray array];
        for (id object in responseObject) {
            CLYPost *post = [CLYPost postWithResponseObject:object];
            [posts addObject:post];
        }
        
        callback(posts);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}


-(void)putPost:(CLYPost*)post image:(UIImage*)image completionHandler:(void (^)(CLYPost *post))callback
{
    
}


-(void) commentOnPost:(NSString*)postId user:(NSString*)userId content:(NSString*)content  completionHandler:(void (^)())callback
{
    NSString *urlString = [NSString stringWithFormat:@"posts/%@/comments", postId];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:content forKey:@"content"];
    [dict setValue:userId forKey:@"user_id"];
    [dict setValue:postId forKey:@"post_id"];
    
    NSDictionary *commentDict = @{@"comment":dict};
    
    [[CLYSessionManager sharedManager] POST:urlString parameters:commentDict success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Success Response: %@", responseObject);
        callback();
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        NSLog(@"ERROR: %@", error);
        callback();
    }];

    
}

-(void)getPostsWithCommunityId:(NSString*)communityId completionHandler:(void (^)(NSArray *posts))callback
{
    NSString *urlString = [NSString stringWithFormat:@"communities/%@/posts/",communityId];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *posts = [NSMutableArray array];
        for (id object in responseObject) {
            CLYPost *post = [CLYPost postWithResponseObject:object];
            [posts addObject:post];
        }
        
        callback(posts);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}

-(void)getStreamWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *posts))callback
{
    NSString *urlString = [NSString stringWithFormat:@"users/%@/stream/",userId];
    
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *posts = [NSMutableArray array];
        for (id object in responseObject) {
            CLYPost *post = [CLYPost postWithResponseObject:object];
            [posts addObject:post];
        }
        
        callback(posts);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}

-(void) markPostAsFavorite:(NSString*)postId forUser:(NSString*)userId
{
    NSString *urlString = [NSString stringWithFormat:@"favorites/%@/?user_id=%@", postId,userId];
    
    [[CLYSessionManager sharedManager] POST:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Success Response: %@", responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        NSLog(@"ERROR: %@", error);
        
    }];
    
    
}

-(void) reportPostAsFlagged:(NSString*)postId user:(NSString*)userId reason:(NSString*)reason comment:(NSString*)comment
{
    NSString *urlString = [NSString stringWithFormat:@"posts/%@/flags?user_id=%@&post_id=%@&reason=%@&comment=%@", postId,userId,postId,[reason stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet],[comment stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
//    NSLog(@"Flag URL String: %@",urlString);
    [[CLYSessionManager sharedManager] POST:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Success Response: %@", responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
//        NSLog(@"ERROR: %@", error);
        
    }];
    
    
}


@end
