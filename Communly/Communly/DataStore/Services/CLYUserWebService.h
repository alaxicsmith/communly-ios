//
//  CLYUserWebService.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLYUser;

@interface CLYUserWebService : NSObject

+ (CLYUserWebService *)sharedService;

-(void)getUserWithId:(NSString*)userId completionHandler:(void (^)(CLYUser *user))callback;
-(void)getCurrentUserWithCompletionHandler:(void (^)(CLYUser *user))callback;
-(void)putUser:(CLYUser*)user avatar:(UIImage*)avatar background:(UIImage*)background completionHandler:(void (^)(CLYUser *user))callback;
-(void)getFeaturedUsersWithCompletionHandler:(void (^)(NSArray *users))callback;
-(void)getNotificationsForCurrentUserWithCompletionHandler:(void (^)(NSArray *notifications))callback;


-(void)createUserWithName:(NSString*)name emailAddress:(NSString*)email username:(NSString*)username password:(NSString*)password completionHandler:(void (^)(NSObject *object))callback;

-(void)followUserWithId:(NSString*)userId;
-(void)unfollowUserWithId:(NSString*)userId;

@property (nonatomic, strong) NSArray *notifications;

@end
