//
//  CLYSearchWebService.m
//  Communly
//
//  Created by Chris Allwein on 4/16/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSearchWebService.h"
#import "CLYSessionManager.h"
#import "CLYCommunity.h"
#import "CLYPost.h"
#import "CLYUser.h"

@implementation CLYSearchWebService

+ (CLYSearchWebService *)sharedService
{
    static CLYSearchWebService *sharedService = nil;
    if(!sharedService)
        sharedService = [[super allocWithZone:nil] init];
    
    return sharedService;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedService];
}

-(void)getSearchResultsWithQuery:(NSString*)searchString completionHandler:(void (^)(NSDictionary *results))callback
{
    NSString *urlString = [NSString stringWithFormat:@"search/?query=%@", [searchString stringByAddingPercentEscapesUsingEncoding:
                                                                           NSASCIIStringEncoding]];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        
        NSMutableArray *communities = [NSMutableArray array];
        NSMutableArray *users = [NSMutableArray array];
        NSMutableArray *posts = [NSMutableArray array];
        
        for (id object in responseObject) {
            if ([object objectForKey:@"community"]) {
                CLYCommunity *community = [CLYCommunity communityWithResponseObject:[object objectForKey:@"community"]];
                [communities addObject:community];
            }
            if ([object objectForKey:@"user"]) {
                CLYUser *user = [CLYUser userWithResponseObject:[object objectForKey:@"user"]];
                [users addObject:user];
            }
            if ([object objectForKey:@"post"]) {
                CLYPost *post = [CLYPost postWithResponseObject:[object objectForKey:@"post"]];
                [posts addObject:post];
            }
        }
        
        NSDictionary *results = @{@"communities":communities,@"users":users,@"posts":posts};
        callback(results);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];

}

@end
