//
//  CLYPostsWebService.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLYPost;

@interface CLYPostsWebService : NSObject

+ (CLYPostsWebService *)sharedService;

-(void)getPostsWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *posts))callback;
-(void)getPostWithId:(NSString*)postId completionHandler:(void (^)(CLYPost *post))callback;
-(void)getPostsWithCommunityId:(NSString*)communityId completionHandler:(void (^)(NSArray *posts))callback;
-(void)putPost:(CLYPost*)post image:(UIImage*)image completionHandler:(void (^)(CLYPost *post))callback;

-(void)getStreamWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *posts))callback;

-(void) createPostWithContent:(NSString *)content withImage:(UIImage *)image forCommunityWithId:(NSString*)communityId byUserWithId:(NSString*)userId;

-(void) commentOnPost:(NSString*)postId user:(NSString*)userId content:(NSString*)content  completionHandler:(void (^)())callback;

-(void) markPostAsFavorite:(NSString*)postId forUser:(NSString*)userId;

-(void) reportPostAsFlagged:(NSString*)postId user:(NSString*)userId reason:(NSString*)reason comment:(NSString*)comment;

@end
