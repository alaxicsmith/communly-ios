//
//  CLYSessionManager.h
//  Communly
//
//  Created by Chris Allwein on 4/2/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@class User;
@class Community;

@interface CLYSessionManager : AFHTTPSessionManager

+ (CLYSessionManager *)sharedManager;

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
    constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

-(void)resetToken;

@end
