//
//  CLYCommunityWebService.m
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYCommunityWebService.h"
#import "CLYSessionManager.h"
#import "CLYCommunity.h"

@implementation CLYCommunityWebService

+ (CLYCommunityWebService *)sharedService
{
    static CLYCommunityWebService *sharedService = nil;
    if(!sharedService)
        sharedService = [[super allocWithZone:nil] init];
    
    return sharedService;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedService];
}

-(void)getCommunitiesWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *communities))callback
{
    NSString *urlString = [NSString stringWithFormat:@"users/%@/neighbor_communities/",userId];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *communities = [NSMutableArray array];
        for (id object in responseObject) {
            CLYCommunity *community = [CLYCommunity communityWithResponseObject:object];
            [communities addObject:community];
        }
        
        callback(communities);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}

-(void)getFeaturedCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback
{
    NSString *urlString = @"featured/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *communities = [NSMutableArray array];
        for (id object in responseObject) {
            CLYCommunity *community = [CLYCommunity communityWithResponseObject:object];
            [communities addObject:community];
        }
        
        callback(communities);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];

}



-(void)getCommunityWithId:(NSString*)communityId completionHandler:(void (^)(CLYCommunity *community))callback
{
    NSString *urlString = [NSString stringWithFormat:@"communities/%@/",communityId];
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        CLYCommunity *community = [CLYCommunity communityWithResponseObject:responseObject];
        callback(community);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }

        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
}



-(void)putCommunity:(Community*)community image:(UIImage*)image completionHandler:(void (^)(CLYCommunity *community))callback
{
    
}

-(void)joinCommunityWithId:(NSString*)communityId
{
    NSString *urlString = [NSString stringWithFormat:@"users/me/neighbor_communities/%@/",communityId];
    [[CLYSessionManager sharedManager] PUT:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Succesfully joined Community: %@", communityId);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JOIN ERROR: %@", error);
    }];
    
}

-(void)unjoinCommunityWithId:(NSString*)communityId
{
    NSString *urlString = [NSString stringWithFormat:@"users/me/neighbor_communities/%@/",communityId];
    [[CLYSessionManager sharedManager] DELETE:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Succesfully unjoined Community: %@", communityId);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"UNJOIN ERROR: %@", error);
    }];
    
}

-(void)getPopularCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback
{
    NSString *urlString = @"communities/popular/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *communities = [NSMutableArray array];
        for (id object in responseObject) {
            CLYCommunity *community = [CLYCommunity communityWithResponseObject:object];
            [communities addObject:community];
        }
        
        callback(communities);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
}

-(void)getVerifiedCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback
{
    NSString *urlString = @"communities/verified/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *communities = [NSMutableArray array];
        for (id object in responseObject) {
            CLYCommunity *community = [CLYCommunity communityWithResponseObject:object];
            [communities addObject:community];
        }
        
        callback(communities);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
}

-(void)getRecentCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback
{
    NSString *urlString = @"communities/recent/";
    [[CLYSessionManager sharedManager] GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //        NSLog(@"Response: %@", responseObject);
        NSMutableArray *communities = [NSMutableArray array];
        for (id object in responseObject) {
            CLYCommunity *community = [CLYCommunity communityWithResponseObject:object];
            [communities addObject:community];
        }
        
        callback(communities);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 401 || response.statusCode ==403) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NEEDS_LOGIN" object:nil];
        }
        
        callback(nil);
        NSLog(@"ERROR: %@", error);
    }];
    
}




@end
