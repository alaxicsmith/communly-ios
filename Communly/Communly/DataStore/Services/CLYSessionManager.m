//
//  CLYSessionManager.m
//  Communly
//
//  Created by Chris Allwein on 4/2/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYSessionManager.h"
#import "AFOAuth2Client.h"
#import "AFURLResponseSerialization.h"

@implementation CLYSessionManager


+ (CLYSessionManager *)sharedManager
{
    static CLYSessionManager *sharedManager = nil;
    if(!sharedManager)
        sharedManager = [[super allocWithZone:nil] init];
    
    return sharedManager;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedManager];
}

- (id)init
{
    self = [super initWithBaseURL:[NSURL URLWithString:@"https://communly.herokuapp.com/api/v1/"]];
    if(self) {
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        AFOAuthCredential *credential =[AFOAuthCredential retrieveCredentialWithIdentifier:@"communly.herokuapp.com"];
        NSString *accessToken = [NSString stringWithFormat:@"token %@",credential.accessToken];
//        NSLog(@"TOken: %@", accessToken);
        [self.requestSerializer setValue:accessToken forHTTPHeaderField:@"Authorization"];
        
    }
    return self;
}

-(void)resetToken
{
    AFOAuthCredential *credential =[AFOAuthCredential retrieveCredentialWithIdentifier:@"communly.herokuapp.com"];
    NSString *accessToken = [NSString stringWithFormat:@"token %@",credential.accessToken];
//    NSLog(@"TOken: %@", accessToken);
    [self.requestSerializer setValue:accessToken forHTTPHeaderField:@"Authorization"];
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
    constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters constructingBodyWithBlock:block error:nil];
    
    __block NSURLSessionDataTask *task = [self uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            if (failure) {
                failure(task, error);
            }
        } else {
            if (success) {
                success(task, responseObject);
            }
        }
    }];
    
    [task resume];
    
    return task;

}



@end
