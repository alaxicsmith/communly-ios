//
//  CLYCommunityWebService.h
//  Communly
//
//  Created by Chris Allwein on 4/3/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLYCommunity;

@interface CLYCommunityWebService : NSObject

+ (CLYCommunityWebService *)sharedService;

-(void)getCommunitiesWithUserId:(NSString*)userId completionHandler:(void (^)(NSArray *communities))callback;
-(void)getCommunityWithId:(NSString*)communityId completionHandler:(void (^)(CLYCommunity *community))callback;
-(void)putCommunity:(CLYCommunity*)community image:(UIImage*)image completionHandler:(void (^)(CLYCommunity *community))callback;
-(void)getFeaturedCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback;

-(void)joinCommunityWithId:(NSString*)communityId;
-(void)unjoinCommunityWithId:(NSString*)communityId;

-(void)getRecentCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback;
-(void)getVerifiedCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback;
-(void)getPopularCommunitiesWithCompletionHandler:(void (^)(NSArray *communities))callback;

@end
