//
//  CLYAsyncDataStore.m
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import "CLYAsyncDataStore.h"
#import <CoreData/CoreData.h>
#import "PersistentStack.h"
#import "CLYPostsWebService.h"
#import "CLYPost.h"


@interface CLYAsyncDataStore ()

    @property (nonatomic, strong) Importer *importer;

@end

@implementation CLYAsyncDataStore

+ (CLYAsyncDataStore *)sharedStore
{
    static CLYAsyncDataStore *sharedStore = nil;
    if(!sharedStore)
        sharedStore = [[super allocWithZone:nil] init];
    
    return sharedStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

- (id)init
{
    self = [super init];
    if(self) {
            
        self.persistentStack = [[PersistentStack alloc] initWithStoreURL:self.storeURL modelURL:self.modelURL];
        
        self.webservice = [[CLYPostsWebService alloc] init];
    }
    return self;
}

- (void)loadNewPosts
{
    [self.webservice getStreamWithUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] completionHandler:^(NSArray *posts)
     {
//         [self.persistentStack.backgroundManagedObjectContext performBlock:^
//          {
//              for(NSDictionary *postDict in posts) {
//                  NSString *identifier = postDict[@"id"];
//                  Post *post = [Post findOrCreatePostWithIdentifier:identifier inContext:self.persistentStack.backgroundManagedObjectContext];
//                  [post loadFromDictionary:postDict];
//              }
//              NSError *error = nil;
//              [self.persistentStack.backgroundManagedObjectContext save:&error];
//              if (error) {
//                  NSLog(@"Error: %@", error.localizedDescription);
//              }
//          }];
         
         NSError *error = nil;
         [self.persistentStack.backgroundManagedObjectContext save:&error];
         if (error) {
             NSLog(@"Error: %@", error.localizedDescription);
         }

     }];
}

-(void) loadPostWithId:(NSString *)postId
{
//    [self.webservice fetchPostWithId:postId completionHandler:^(NSDictionary *postDict)
//     {
//         [self.persistentStack.backgroundManagedObjectContext performBlock:^
//          {
//              NSString *identifier = postId;
//              Post *post = [Post findOrCreatePostWithIdentifier:identifier inContext:self.persistentStack.backgroundManagedObjectContext];
//              [post loadFromDictionary:postDict];
//        
//          NSError *error = nil;
//              [self.persistentStack.backgroundManagedObjectContext save:&error];
//              if (error) {
//                  NSLog(@"Error: %@", error.localizedDescription);
//              }
//          }];
//     }];
}

- (void)saveContext
{
    NSError *error = nil;
    [self.persistentStack.managedObjectContext save:&error];
    if (error) {
        NSLog(@"error saving: %@", error.localizedDescription);
    }
}

- (NSURL*)storeURL
{
    NSURL* documentsDirectory = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                                       inDomain:NSUserDomainMask
                                                              appropriateForURL:nil
                                                                         create:YES
                                                                          error:NULL];
    return [documentsDirectory URLByAppendingPathComponent:@"db2.sqlite"];
}

- (NSURL*)modelURL
{
    return [[NSBundle mainBundle] URLForResource:@"CLYModel" withExtension:@"momd"];
}

-(NSManagedObjectContext*) getManagedObjectContext
{
    return self.persistentStack.managedObjectContext;
}

-(NSManagedObjectContext*) getBackgroundManagedObjectContext
{
    return self.persistentStack.backgroundManagedObjectContext;
}

@end
