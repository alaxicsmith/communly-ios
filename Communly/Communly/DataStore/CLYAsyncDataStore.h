//
//  CLYAsyncDataStore.h
//  Communly
//
//  Created by Chris Allwein on 3/27/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PersistentStack;
@class CLYPostsWebService;
@class Importer;

@interface CLYAsyncDataStore : NSObject
{
    
}

+ (CLYAsyncDataStore *)sharedStore;
- (void)saveContext;
- (void)loadNewPosts;


@property (nonatomic, strong) PersistentStack *persistentStack;
@property (nonatomic, strong) CLYPostsWebService *webservice;

-(NSManagedObjectContext*) getManagedObjectContext;
-(NSManagedObjectContext*) getBackgroundManagedObjectContext;

@end
