//
//  main.m
//  Communly
//
//  Created by Chris Allwein on 3/10/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CLYAppDelegate class]));
    }
}
