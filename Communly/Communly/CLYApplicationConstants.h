//
//  CLYApplicationConstants.h
//  Communly
//
//  Created by Chris Allwein on 3/25/14.
//  Copyright (c) 2014 Downright Simple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLYApplicationConstants : NSObject

+(UIColor*) applicationBackgroundColor;
+(UIColor*) menuBackgroundColor;
+(UIColor*) redButtonColor;
+(UIColor*) applicationTransparentBackgroundColor;

+(NSString*) client_id;
+(NSString*) client_secret;


@end
